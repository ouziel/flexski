import React, {PropTypes as T} from 'react';
import Navbar from 'components/Navbar'
import {observer} from 'mobx-react'
import Snackbar from 'react-toolbox/lib/snackbar';
import Footer from 'pages/shared/Footer'

@observer(['ui'])
 class App extends React.Component {

        render() {
					const {children, location,ui} = this.props
          const {message, showMsg, onTimeout} = ui
            return (
                <div>
                  <Navbar pathname={location.pathname}/>
                  <main>
                  {React.cloneElement(children, {key: location.pathname})}
                  </main>
                  <Snackbar active={showMsg} type={'warning'} label={message} timeout={2000} onTimeout={onTimeout}/>
                  <Footer path={location.pathname}/>
								</div>
            )
        }


}
module.exports = App