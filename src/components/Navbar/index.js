import React, {PropTypes} from 'react';
import AppBar from 'react-toolbox/lib/app_bar';
import {Button, IconButton} from 'react-toolbox/lib/button';
import Navigation from 'react-toolbox/lib/navigation';
import {Link} from 'react-router'
import CSSModules from 'react-css-modules'
import styles from './styles.scss'
import Drawer from 'react-toolbox/lib/drawer';
import { List, ListItem, ListSubHeader, ListDivider, ListCheckbox } from 'react-toolbox/lib/list';
import {withRouter} from 'react-router'
import texts from 'config/texts'
import {toJS} from 'mobx'
import {observer} from 'mobx-react'
import Logo from 'components/Svgs/Logo'

// const Logo = () => {
//   return (
//     <div className={styles.logo}>
//       <img src={require('assets/svg/logo.svg')} className={styles.logoImg}/>
//     </div>
//   )
// }
//

@withRouter
@CSSModules(styles)
@observer(['session','ui'])
export default class Navbar extends React.Component {

constructor(props) {
  super(props)
  this.state = {
  drawerActive: false,
  hideNavLogo: true,
};
this.checkScrollHeight = this.checkScrollHeight.bind(this)
}

handleToggle = () => {
  this.setState({drawerActive: !this.state.drawerActive});
};


checkScrollHeight = () => {
    const {scrollY} = window
    if (scrollY > 75) {
      this.setState({hideNavLogo: false})
    } else {
      this.setState({hideNavLogo: true})
    }
}

componentWillReceiveProps(props) {
    if (!props.ui.isHome) {
    this.setState({hideNavLogo: false})
    window.removeEventListener('scroll', this.checkScrollHeight,true)
  }
  else {
      this.initScrollTrack();
  }
}
initScrollTrack = () => {
    this.setState({hideNavLogo: true})
  window.addEventListener('scroll', this.checkScrollHeight,true)
}
componentDidMount() {
  const {appbar} = this.refs;
  const {pathname,ui} = this.props
  const headerHeight = window.getComputedStyle(appbar.parentNode).height
  ui.headerHeight = parseFloat(headerHeight)
  if (ui.isHome) {
    this.initScrollTrack();
  }
  else {
        this.setState({hideNavLogo: false})
  }
}

navigate = id => {
  this.handleToggle();
  this.props.router.replace(id)
}
  render() {
    const {navigate} = this
    const isAdmin = toJS(this.props.session.isAdmin)
    const {hideNav,isHome} = this.props.ui
    const {hideNavLogo}  = this.state
    const appBarClass = isHome ? hideNavLogo ?  styles.navBlank : styles.appBar : hideNav ? styles.hideNav : styles.appBar
     return (
      <AppBar fixed={true} className={appBarClass} theme={styles}>
      <div className={styles.inner} ref={'appbar'}>
            <Navigation className={styles.navigation} theme={styles}>
              <Link to={'/'} className={styles.link}>
                <Button label={texts.home} className={styles.pageButton} theme={styles}/>
              </Link>
              <Link to={'/wizard'} className={styles.link}>
                <Button label={texts.wizard} className={styles.pageButton} theme={styles}/>
              </Link>
              <Link to={'/about'} className={styles.link}>
                <Button label={texts.about} className={styles.pageButton} theme={styles}/>
              </Link>
              <Link to={'/contact'} className={styles.link}>
                <Button label={texts.contactUs} className={styles.pageButton} theme={styles}/>
              </Link>
              {isAdmin && <Link to={'/admin/dashboard'} className={styles.link}>
                  <Button label={texts.admin.dashboard} className={styles.pageButton} theme={styles}/>
                </Link>}
            </Navigation>
            <div className={styles.logoBox}>
            {!hideNavLogo ?   <Logo svgClass={styles.logoSmall}/> :   <Logo svgClass={styles.logoBig}/> }

            </div>
      </div>
      <IconButton icon={this.state.drawerActive ? 'close' : 'menu'} className={styles.toggleBtn} theme={styles} onClick={this.handleToggle}/>

        <Drawer type={'left'} className={styles.navDrawer} theme={styles} active={this.state.drawerActive} onOverlayClick={this.handleToggle}>

          <div className={styles.centerLogo}>
                  <Logo svgClass={styles.logoSvgDrawer}/>
          </div>
        <br/>
                <ListDivider />
        <List selectable ripple>
        <ListItem
      className={styles.navItem}
      theme={styles}
      caption={texts.home}
      leftIcon='home'
      onClick={this.navigate.bind(this,'/')}
    />
    <ListItem
  className={styles.navItem}
  theme={styles}
  caption={texts.wizard}
  leftIcon='description'
  onClick={this.navigate.bind(this,'/wizard')}
/>
    <ListItem
    className={styles.navItem}
    theme={styles}
      caption={texts.about}
      leftIcon='info'
      onClick={this.navigate.bind(this,'/about')}
    />
    <ListItem
    className={styles.navItem}
    theme={styles}
    leftIcon='email'
      caption={texts.contactUs}
      onClick={this.navigate.bind(this,'/contact')}
    />


        {/*
          // drawer doesnt open on mobile when this is on

          {isAdmin &&   <ListItem
          className={styles.navItem}
          theme={styles}
          leftIcon='dashboard'
            caption={texts.admin.dashboard}
            onClick={this.navigate.bind(this,'/admin/dashboard')}
          />} */}
  </List>
       </Drawer>
      </AppBar>
    );
  }
}

