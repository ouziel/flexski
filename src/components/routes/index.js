import 'babel-polyfill'
import React from 'react'
import {
    Router,
    Link,
    Route,
    IndexRoute,
    browserHistory,
    useRouterHistory
} from 'react-router'
import styles from '!style!css!sass!styles/global.scss'
import {observer, Provider} from 'mobx-react'
import {autorun, observable} from 'mobx'
import {Session, ui, wizard} from 'stores'
// import {HomePage, AboutPage, ContactPage, WizardPage, UserDetailsPage, TripDetailsPage} from 'pages'
/* PAGES */
import App from 'components/App'
import firebaseConfig from 'config/firebase'
import AboutPage from 'pages/about'
import HomePage from 'pages/home'
import ContactPage from 'pages/contact'
import WizardPage from 'pages/wizard'
import UserDetailsPage from 'pages/wizard/UserDetails'
import TripDetailsPage from 'pages/wizard/TripDetails'
import AuthPage from 'pages/admin/auth'
import DashboardPage from 'pages/admin/Dashboard'
import AdminLeadsList from 'pages/admin/Leads/list'
import AdminLeadsSingle from 'pages/admin/Leads/single'
import AdminQuoteSingle from 'pages/admin/Leads/quote'
import AdminPostsList from 'pages/admin/Posts/list'
import AdminPostsSingle from 'pages/admin/Posts/single'
import AdminGroupList from 'pages/admin/Groups/AdminList'
import SingleGroup from 'pages/admin/Groups/Single'
import NoMatch from 'pages/NoMatch'
import TermsPage from 'pages/terms'
import counterpart from 'counterpart'
import ReactGA from 'react-ga'
ReactGA.initialize('UA-87415373-1')

// const createHistory = require('history/lib/createBrowserHistory') const history = useRouterHistory(createHistory)({basename: '/'});
const hideLoader = require('appUtil/hideLoader')
function lazyLoadComponent (lazyModule) {
  return (location, cb) => {
    lazyModule(module => {
      cb(null, module)
    })
  }
}

function handleRouteChange ({ui, path}) {
  ui.hideNav = false
  document.body.scrollTop = 0
  hideLoader.default()
  ui.pageTitle = counterpart(path.pathname.replace('/', '.'))
  if (path.pathname === '/') {
    ui.isHome = true
  } else {
    ui.isHome = false
  }
}

function logPageView () {
  ReactGA.set({ page: window.location.pathname })
  ReactGA.pageview(window.location.pathname)
}

export default class FlexskiApp extends React.Component {

  constructor (props) {
    super(props)
    const session = new Session(firebaseConfig)
    this.stores = {
      session,
      ui,
      wizard
    }
    browserHistory.listen(path => handleRouteChange({ui, path}))
  }

  render () {
    return (
      <Provider {...this.stores}>
        <Router history={browserHistory} onUpdate={logPageView}>
          <Route path={'/'} component={App}>
            <IndexRoute getComponent={lazyLoadComponent(HomePage)} />
            <Route path={'/about'} getComponent={lazyLoadComponent(AboutPage)} />
            <Route path={'/wizard'}>
              <IndexRoute getComponent={lazyLoadComponent(WizardPage)} />
              <Route path={'/wizard/trip'} getComponent={lazyLoadComponent(TripDetailsPage)} />
              <Route path={'/wizard/user'} getComponent={lazyLoadComponent(UserDetailsPage)} />
            </Route>
            <Route path={'/contact'} getComponent={lazyLoadComponent(ContactPage)} />
            <Route path={'/auth'} getComponent={lazyLoadComponent(AuthPage)} />

            <Route path={'/admin'} onEnter={this.stores.session.requireAdmin}>
              <IndexRoute getComponent={lazyLoadComponent(AuthPage)} />
              <Route path={'/admin/dashboard'} getComponent={lazyLoadComponent(DashboardPage)} />
              <Route path={'/admin/leads'}>
                <IndexRoute getComponent={lazyLoadComponent(AdminLeadsList)} />
                <Route path={'/admin/leads/:id'} getComponent={lazyLoadComponent(AdminLeadsSingle)} />
              </Route>
              <Route path={'/admin/quote'}>
                <IndexRoute getComponent={lazyLoadComponent(AdminQuoteSingle)} onEnter={(s, r) => r('/')} />
                <Route path={'/admin/quote/:id'} getComponent={lazyLoadComponent(AdminQuoteSingle)} />
              </Route>

              {/* </Route> */}
              <Route path={'/admin/groups'} getComponent={lazyLoadComponent(AdminGroupList)} />

              <Route path={'/admin/posts'}>
                <IndexRoute getComponent={lazyLoadComponent(AdminPostsList)} />
                <Route path={'/admin/posts/:id'} getComponent={lazyLoadComponent(AdminPostsSingle)} />
              </Route>

            </Route>

            <Route path={'/groups'}>
              <IndexRoute getComponent={lazyLoadComponent(SingleGroup)} onEnter={(s, r) => r('/')} />
              <Route path={'/groups/:id'} getComponent={lazyLoadComponent(SingleGroup)} />
            </Route>
            <Route path={'/terms'} getComponent={lazyLoadComponent(TermsPage)} />

            <Route path='*' component={NoMatch} />
          </Route>
        </Router>
      </Provider>
    )
  }

}
