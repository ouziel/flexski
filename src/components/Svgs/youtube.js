import React, {PropTypes as T} from 'react';
import CSSModules from 'react-css-modules'
import styles from './styles.scss'

export default class YoutubeIcon extends React.Component {
  static propTypes = {
    iconClass: T.string,
    circleClass: T.string,
    svgClass: T.string,
  }
  static defaultProps = {
    iconClass: styles.icon,
    svgClass: styles.svg,
    circle: styles.circle,
  }
    state = {
      hover: false,
      icon: this.props.iconClass,
      svg: this.props.svgClass,
      circle: this.props.circleClass,
    }

handleHover = () => {
  this.setState({hover: !this.state.hover})
}
  render() {
    const {svgClass, iconClass , circleClass } = this.props
    const {hover} = this.state;
    let svg = svgClass, circle = circleClass, icon = iconClass;
    if (hover) {
      icon = this.state.circle
      circle = this.state.icon
    }
    return (
      <svg  viewBox="0 0 34 34" className={svg} onMouseOver={this.handleHover.bind(this)} onMouseOut={this.handleHover.bind(this)} >
      <path   className={icon}   d="M17,34c9.4,0,17-7.6,17-17c0-9.4-7.6-17-17-17C7.6,0,0,7.6,0,17C0,26.4,7.6,34,17,34"/>
      <path className={circle} d="M20.3,17l-5.6,3.3v-6.5L20.3,17z M25.5,20V14c0,0,0-2.9-2.9-2.9H11.4c0,0-2.9,0-2.9,2.9V20c0,0,0,2.9,2.9,2.9
      	h11.1C22.6,23,25.5,23,25.5,20"/>
      </svg>

    )
  }
}





