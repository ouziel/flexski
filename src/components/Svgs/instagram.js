import React, {PropTypes as T} from 'react';

import CSSModules from 'react-css-modules'
import styles from './styles.scss'

export default class InstaIcon extends React.Component {
  static propTypes = {
    iconClass: T.string,
    circleClass: T.string,
    svgClass: T.string,
  }
  static defaultProps = {
    iconClass: styles.icon,
    svgClass: styles.svg,
    circle: styles.circle,
  }
    state = {
      hover: false,
      icon: this.props.iconClass,
      svg: this.props.svgClass,
      circle: this.props.circleClass,
    }

handleHover = () => {
  this.setState({hover: !this.state.hover})
}
  render() {
    const {svgClass, iconClass , circleClass } = this.props
    const {hover} = this.state;
    let svg = svgClass, circle = circleClass, icon = iconClass;
    if (hover) {
      icon = this.state.circle
      circle = this.state.icon
    }
    return (
      <svg  viewBox="0 0 34 34" className={svgClass} onMouseOver={this.handleHover.bind(this)} onMouseOut={this.handleHover.bind(this)} >
      <path className={icon} d="M17,34c9.4,0,17-7.6,17-17c0-9.4-7.6-17-17-17C7.6,0,0,7.6,0,17C0,26.4,7.6,34,17,34"/>
      <path  className={circle} d="M22.2,8.5H11.8c-1.8,0-3.3,1.5-3.3,3.3v3.5v7c0,1.8,1.5,3.3,3.3,3.3h10.4c1.8,0,3.3-1.5,3.3-3.3v-7v-3.5
      	C25.5,10,24,8.5,22.2,8.5z M23.2,10.5l0.4,0v0.4v2.5l-2.9,0l0-2.9L23.2,10.5z M14.6,15.3C15.1,14.5,16,14,17,14c1,0,1.9,0.5,2.4,1.2
      	c0.4,0.5,0.6,1.1,0.6,1.7c0,1.7-1.3,3-3,3s-3-1.3-3-3C14,16.4,14.2,15.8,14.6,15.3z M23.9,22.2c0,0.9-0.7,1.6-1.6,1.6H11.8
      	c-0.9,0-1.6-0.7-1.6-1.6v-7h2.5c-0.2,0.5-0.3,1.1-0.3,1.7c0,2.6,2.1,4.7,4.7,4.7c2.6,0,4.7-2.1,4.7-4.7c0-0.6-0.1-1.2-0.3-1.7h2.5
      	V22.2z"/>
      </svg>

    )
  }
}





