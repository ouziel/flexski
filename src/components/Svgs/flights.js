import React, {PropTypes} from 'react';
import CSSModules from 'react-css-modules'
import styles from './styles.scss'
const FlightsIcon = CSSModules(({svgClass = styles.svg, iconClass = styles.icon, circleClass = styles.circle} = {}) => {
  return (
    <svg viewBox="0 0 173.5 173.5" className={svgClass}>
    <g>
    	<circle  className={circleClass} cx="86.8" cy="86.8" r="86.8"/>
    	<path className={iconClass} d="M106.5,94.6c-26.3,15.5-54.2,26.7-58,22.4l-14.1-11.5l5.3-3.1l15.9,3.1
    		c5.4-5.1,13.2-11.3,22.3-17.5L46.1,64.3l9-5.3l48.7,13c21.2-11.5,30-11.7,32.6-7.2C139.3,69.8,133.3,78.8,106.5,94.6z M109.6,96.2
    		l-7.2,16.4l-7.8,4.1l-3.9-10c5.2-2.7,10.5-5.6,15.8-8.7C107.5,97.5,108.6,96.8,109.6,96.2z"/>
    </g>
    </svg>
  )
},styles)

export default FlightsIcon

