import React, {PropTypes} from 'react';
import CSSModules from 'react-css-modules'
import styles from './styles.scss'

const LocationIcon = CSSModules(({svgClass = styles.svg, iconClass = styles.icon, circleClass = styles.circle} = {}) => {
  return (
    <svg viewBox="0 0 173.5 173.5" className={svgClass}>

    <g>
    	<g>
    		<circle className={circleClass} cx="86.8" cy="86.8" r="86.8"/>
    		<path className={iconClass} d="M101.3,119.7c-13.1,0-27.6-5.2-39.8-5.2c-15.3,0-28.5,5.2-28.5,5.2l1.7-5.2
    			c0,0,11.4-3.5,26.7-3.5c12.2,0,26.8,5.2,39.9,5.2c19.5,0,35.8-5.2,35.8-5.2l1.7,3.5C138.8,114.5,120.8,119.7,101.3,119.7z
    			 M127.5,76.3c-4.3,0-7.8-3.5-7.8-7.8s3.5-7.8,7.8-7.8c4.3,0,7.8,3.5,7.8,7.8S131.8,76.3,127.5,76.3z M127.3,91
    			c3.1,5.6,8.1,14.8,8.1,14.8s-14.5,5.2-34.1,5.2c-12.8,0-27-5.2-39-5.2c-15.6,0-25.8,3.5-25.8,3.5s12.1-22.1,17.8-32.7
    			c1.7-3,4.9-3.7,7.5-0.2c0.8,0.9,1.6,1.9,2.4,2.8c1.8,2.1,4,3.3,5.8,0.1c4-6.6,12.2-20,17.3-28.5c2.3-4.1,6.4-3.6,8.3,0
    			c5.2,10.8,14.6,30.3,18.7,38.7c1.3,2.7,2.9,3.5,5,1.6c0.6-0.5,1.2-0.9,1.7-1.4C123.3,87.9,125.8,88.4,127.3,91z"/>
    	</g>
    	<rect className={iconClass} x="44.5" y="49.8" class="st2" width="6.6" height="9"/>
    </g>
    </svg>

  )
},styles)

export default LocationIcon

