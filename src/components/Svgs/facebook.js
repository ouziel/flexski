import React, {PropTypes as T} from 'react';
import CSSModules from 'react-css-modules'
import styles from './styles.scss'


export default class FacebookIcon extends React.Component {
  static propTypes = {
    iconClass: T.string,
    circleClass: T.string,
    svgClass: T.string,
  }
  static defaultProps = {
    iconClass: styles.icon,
    svgClass: styles.svg,
    circle: styles.circle,
  }
    state = {
      hover: false,
      icon: this.props.iconClass,
      svg: this.props.svgClass,
      circle: this.props.circleClass,
    }

handleHover = () => {
  this.setState({hover: !this.state.hover})
}
  render() {
    const {svgClass, iconClass , circleClass } = this.props
    const {hover} = this.state;
    let svg = svgClass, circle = circleClass, icon = iconClass;
    if (hover) {
      icon = this.state.circle
      circle = this.state.icon
    }
    return (
      <svg  viewBox="0 0 34 34" className={svgClass} onMouseOver={this.handleHover.bind(this)} onMouseOut={this.handleHover.bind(this)} >

      <path   id="icon" className={icon} d="M17,34c9.4,0,17-7.6,17-17c0-9.4-7.6-17-17-17C7.6,0,0,7.6,0,17C0,26.4,7.6,34,17,34"/>
      <path id="circle" className={circle} d="M12.9,14.1h1.8v-1.7c0-0.8,0-1.9,0.6-2.6C15.8,9,16.6,8.5,18,8.5c2.2,0,3.2,0.3,3.2,0.3l-0.4,2.6
      	c0,0-0.7-0.2-1.4-0.2c-0.7,0-1.3,0.2-1.3,0.9v2h2.8l-0.2,2.5H18v8.8h-3.3v-8.8h-1.8V14.1z"/>
      </svg>

    )
  }
}





