import React, {PropTypes} from 'react';
import CSSModules from 'react-css-modules'
import styles from './styles.scss'
const DatesIcon = CSSModules(({svgClass = styles.svg, iconClass = styles.icon, circleClass = styles.circle} = {}) => {
  return (
    <svg viewBox="0 0 173 173.5" className={svgClass}>
    <g>
    	<rect x="70.6" y="71.4" className={iconClass} width="14" height="9.5"/>
    	<rect x="52.7" y="84.7" className={iconClass} width="14" height="9.5"/>
    	<rect x="70.6" y="84.7" className={iconClass} width="14" height="9.5"/>
    	<rect x="52.7" y="98.1" className={iconClass} width="14" height="9.5"/>
    	<rect x="70.6" y="98.1" className={iconClass} width="14" height="9.5"/>
    	<rect x="65.4" y="99.6" className={iconClass} width="12.4" height="8.5"/>
    	<g>
    		<ellipse className={circleClass} cx="86.5" cy="86.8" rx="86.5" ry="86.8"/>
    		<rect x="36.3" y="42.2" className={iconClass} width="100.4" height="92.5"/>
    		<rect x="43.1" y="65.4" className={circleClass} width="86.8" height="62.5"/>
    		<rect x="97.8" y="62.9" className={iconClass} width="13" height="8.9"/>
    		<rect x="114.5" y="62.9" className={iconClass} width="13" height="8.9"/>
    		<rect x="97.8" y="75.3" className={iconClass} width="13" height="8.9"/>
    		<rect x="114.5" y="75.3" className={iconClass} width="13" height="8.9"/>
    		<rect x="97.8" y="87.8" className={iconClass} width="13" height="8.9"/>
    		<rect x="114.5" y="87.8" className={iconClass} width="13" height="8.9"/>
    		<rect x="81.1" y="100.3" className={iconClass} width="13" height="8.9"/>
    		<rect x="97.8" y="100.3" className={iconClass} width="13" height="8.9"/>
    	</g>
    	<rect x="45.1" y="50.3" className={circleClass} width="41.4" height="10.7"/>
    	<rect x="126.2" y="31.9" className={iconClass} width="6.2" height="8.5"/>
    </g>
    </svg>
  )
},styles)

export default DatesIcon