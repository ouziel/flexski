import {observable, autorun, action, toJS} from 'mobx'
import firebase from 'firebase';
window.firebase = firebase

class Session {

    constructor(config) {
        if (!config)
            throw new Error('no firebase config')
        this.authed = observable(false)
        this.uid = observable('uid')
        this.isAnon = observable(false)
        this.isAdmin = observable(false)

        firebase.initializeApp(config);
        firebase.auth().onAuthStateChanged(user => {
            if (!user) {
                firebase.auth().signInAnonymously().catch(error => {
                    const errorMessage = error.message;
                    console.log(errorMessage)
                });
            } else {
                this.isAnon = user.isAnonymous
                this.uid = user.uid
                this.authed = true
                this.verifyAdmin(user.uid).then(snap => {
                    this.isAdmin = snap.val()
                }).catch(err => this.isAdmin = false)
            }
        })
    }
    requireAdmin = (state, replace) => {
        if (toJS(this.isAdmin) !== true) {
            replace('/')
        }
    }
    verifyAdmin(uid) {
        const ref = firebase.database().ref().child('admins').child(uid)
        return ref.once('value')

    }
}

export default Session