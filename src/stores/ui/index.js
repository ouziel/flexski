import {observable} from 'mobx'
class UI {
    @observable drawer = false
    @observable dimmer = false
    @observable showMsg = false
    @observable message = ''
    @observable msgType = ''
    @observable lang = 'en'
    @observable isMobile = false
    @observable headerHeight = 0
    @observable hideNav = false
    @observable pageTitle = ''

    constructor() {
        window.onresize = this.handleResize
        window.onresize()
    }
    set(key, value) {
        this[key] = value
    }
    showError = err => {
        this.showMsg = true;
        this.message = err;
    }
    handleResize = (e) => {
        const {innerWidth} = window
        if (innerWidth <= 768) {
            this.isMobile = true;
        } else {
            this.isMobile = false;
        }
    }
    onTimeout = () => {
        this.showMsg = false
        setTimeout(() => {
            this.message = ''
        }, 3500)
    }
}

const ui = new UI()
export default ui
