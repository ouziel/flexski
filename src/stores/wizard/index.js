import {observable, toJS} from 'mobx'
var uniqid = require('uniqid');

class Wizard {

  @observable startedTripDetails = false
  @observable finishedTripDetails = false
  @observable tripDetails = {
   country: '', budget: 0,  isFlexable: false, children: 0, adults: 0, accomndation: ''
  }
  @observable userDetails = {}

  reset = () => {
    this.tripDetails = {}
    this.userDetails = {}
    this.startedTripDetails = false
    this.finishedTripDetails = false
  }

}

const wizard = new Wizard()
export default wizard
