import React, {PropTypes} from 'react';
import styles from './styles.scss'
import {Link} from 'react-router'
import counterpart from 'counterpart'
const pageWithoutFooter = [
  '/','home',
]
export default class Footer extends React.Component {
  render() {
    const {path} = this.props
    const hide =  pageWithoutFooter.includes(path) || pageWithoutFooter.some(x => x=== path)
    const footerClass = hide ? styles.hide : styles.footer
    return (<footer className={footerClass}>
      <div className={styles.footerInner}>
        <span><a  className={styles.devBrand} target="_blank" href="http:///www.esnc.io/?ref='flexski'">ESNC</a></span>
        <div className={styles.brand}>
          <Link to={'/terms'}>{counterpart('app.terms')}</Link>
          <span>Flexski &copy; </span>
        </div>
      </div>
    </footer>);
  }
}

