import React, {PropTypes} from 'react';
import DatesIcon from 'components/Svgs/dates'
import FlightsIcon from 'components/Svgs/flights'
import HotelIcon from 'components/Svgs/hotel'
import LocationIcon from 'components/Svgs/locations'
import TransportIcon from 'components/Svgs/transport'
import CSSModules from 'react-css-modules'
import styles from './style.scss'
import {observer} from 'mobx-react'
const Slider = require('react-slick');
import {toJS} from 'mobx'


@CSSModules(styles)
@observer(['ui'])
export default class Grid extends React.Component {
  render() {
          const {isMobile} = this.props.ui

    return (

        <section className={styles.section}>
        <h1 className={styles.title}>{'אפשרויות החבילה'}</h1>
        {/* grid  */}
        <ul className={styles.list}>

              {data.map((item,i) => {
                const Icon = item.icon
                return (
                  <li key={i} className={styles.item}>
                      <Icon  circleClass={item.circleClass}/>
                      <h4 className={styles.itemTitle}>{item.title}</h4>
                      <p  className={styles.itemText}>{item.text}</p>
                  </li>
                )
              })}

          </ul>

        </section>
    );
  }
}



