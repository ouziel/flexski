import React, {PropTypes as T} from 'react';
import CSSModules from 'react-css-modules'
import styles from './style.scss'
import {observer} from 'mobx-react'
import {Link} from 'react-router'
import counterpart from 'counterpart'
import {IconButton} from 'react-toolbox/lib/button';


@CSSModules(styles)
@observer(['ui'])
class Page extends React.Component {
    static propTypes = {
      name: T.string,
      nextLink: T.string,
      prevLink: T.string,
      innerClass: T.string,
      hideNavOnScroll: T.bool,
      cols: T.bool,
      prevFunc: T.oneOf([T.bool, T.func])

    }
    static defaultProps = {
      name: 'name',
      nextLink: null,
      prevLink: null,
      hideNavOnScroll: true,
      innerClass: null,
      prevFunc: false,
    }

    componentDidMount() {


      const {hideNavOnScroll} = this.props
      if (hideNavOnScroll) {
        window.addEventListener('scroll', this.checkScrollHeight,true)
      }
      else {
        window.removeEventListener('scroll', this.checkScrollHeight,true)
      }

    }
    checkScrollHeight = () => {
        const {scrollY} = window
        const {headerHeight} = this.props.ui
        if (scrollY > headerHeight) {

            this.props.ui.hideNav = true
        } else {
            this.props.ui.hideNav = false
        }
    }
    componentWillUnmount() {
      window.removeEventListener('scroll', this.checkScrollHeight,true)
    }


    navigate = id => this.props.router.replace(id)
    render() {

      const {children, prevFunc, nextLink, prevLink, innerClass, cols, name} = this.props
      const coverClass = this.props.ui.hideNav
          ? styles.coverOn
          : styles.cover
      const innerClassName = innerClass ? innerClass : cols ? styles.innerCols : styles.inner
        return (
          <div className={styles.wrapper}>
              <section className={coverClass}>
                  <div className={styles.coverInner}>
                    { prevLink && <Link to={prevLink} className={styles.arrowLinkRight}><IconButton icon={'arrow_forward'} className={'arrowForward'} theme={styles}/></Link>}
                  { prevFunc && <span className={styles.arrowLinkRight}><IconButton icon={'arrow_forward'} className={'arrowForward'} theme={styles} onClick={prevFunc}/></span>}
                  <h4>{counterpart(`${name}.title`)}</h4>
                  { nextLink && <Link to={nextLink} className={styles.arrowLinkLeft}><IconButton icon={'arrow_back'} className={'arrowBack'} theme={styles}/></Link>}
                  </div>
              </section>

                <section className={innerClassName}>
                  {children}
                </section>
            </div>
        );
    }
}

module.exports = Page