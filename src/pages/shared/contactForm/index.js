import React, {PropTypes as T} from 'react';
import CSSModules from 'react-css-modules'
import styles from './style.scss'
import Input from 'react-toolbox/lib/input';
import {Button, IconButton} from 'react-toolbox/lib/button';
import validateInputs from 'appUtil/validateInputs.js'
import ProgressBar from 'react-toolbox/lib/progress_bar';
import Checkbox from 'react-toolbox/lib/checkbox';
import counterpart from 'counterpart'
import {Link} from 'react-router'

const  extrasHint = ``
const inputs = {
    name: {
        fields: ['name'],
        validate(x) {
            return (x)
        }
    },
    tel: {
        fields: ['tel'],
        validate(x) {
            return (x)
        }
    },
    email: {
        fields: ['email'],
        validate(x) {
            return (x)
        }
    },
    msg: {
        fields: ['msg'],
        validate(x) {
            return (x)
        }
    }
}
const LicenceLabel = () => {
  return (
    <div className={styles.terms}>
      <span>{counterpart('app.approveTerms')}</span>
      <Link to={'/terms'} className={styles.link}>{counterpart('app.terms')}</Link>
    </div>
  )
}
@CSSModules(styles)
export default class ContatForm extends React.Component {
    static propTypes = {
        handleSubmit: T.func,
        isStep: T.bool,
        pending: T.bool,
        inputs: T.object,
    }
    static defaultProps = {
        isStep: false,
        pending: false,
        inputs: inputs
    }
    state = {
        name: '',
        tel: '',
        email: '',
        msg: '',
        approvedTerms: false,
        hint: counterpart('wizard.extrasHint'),
    };

    handleChange = (name, value) => {
        this.setState({
            ...this.state,
            [name]: value
        });
      if (name === 'msg' && value) {
        this.setState({hint: ''})
      }
      else if (name === 'msg' && !value) {
        this.setState({hint: counterpart('wizard.extrasHint')})
      }
      if (this.props.updateStore) {
        this.props.updateStore(name,value)
      }
    };
    componentDidMount() {
        this.setData();
    }
    componentWillReceiveProps() {
      this.setData();
    }
    setData = () => {
      const {data} = this.props
      if (data) {
        this.setState({...data})
      }
    }
    handleSubmit = (e) => {
        e.preventDefault();
        const shouldUpdateState = validateInputs(this, this.props.inputs)
        if (shouldUpdateState) {
            this.props.handleSubmit(this.state)
        }
    }
    render() {
        const {isStep,pending} = this.props
        return (

            <form onSubmit={this.handleSubmit} className={styles.form}>
            {isStep && <h4 className={this.props.pending ? `${styles.pending} ${styles.title}` : styles.title }>{'פרטי התקשרות'}</h4>}
                <div>
                    <Input ref={'name'} type='text' icon="account_box" label='שם מלא' name='name'
                    disabled={this.props.pending}
                    value={this.state.name} onChange={this.handleChange.bind(this, 'name')}    className={this.props.pending ? `${styles.pending} ${styles.formInput}` : styles.formInput } theme={styles}/>
                </div>
                <div >
                    <Input ref={'email'} type='email' label='דוא״ל' icon='email'
                    disabled={this.props.pending}
                    value={this.state.email} onChange={this.handleChange.bind(this, 'email')}       className={this.props.pending ? `${styles.pending} ${styles.formInput}` : styles.formInput } theme={styles}/>
                </div>
                <div>
                    <Input ref={'tel'} type='tel' label='טלפון' name='tel'
                    disabled={this.props.pending}
                    icon='phone' value={this.state.tel} onChange={this.handleChange.bind(this, 'tel')}       className={this.props.pending ? `${styles.pending} ${styles.formInput}` : styles.formInput } theme={styles}/>
                </div>
                {isStep && <h4 className={this.props.pending ? `${styles.pending} ${styles.title}` : styles.title }>{'בקשות נוספות'}</h4>}
                <div>
                    <Input ref={'msg'} type='text' label='הודעה' name='msg' icon='message'
                    disabled={this.props.pending}
                    value={this.state.msg} onChange={this.handleChange.bind(this, 'msg')}
                     hint={isStep ? this.state.hint : ''} multiline
                    className={this.props.pending ? `${styles.pending} ${styles.formInput}` : styles.formInput }
                     theme={styles} maxLength={150}
                    />
                </div>
              {isStep &&   <div>
                  <Checkbox
                      ref={'approvedTerms'}
                      checked={this.state.approvedTerms}
                      label={<LicenceLabel/>}
                      onChange={this.handleChange.bind(this, 'approvedTerms')}
                      className={styles.approvedTerms}
                      theme={styles}/>
                </div>}
                <div className={styles.margin}>
                    <input type="submit" hidden/>
                  <ProgressBar type="linear" mode="indeterminate" className={this.props.pending ? ` ${styles.visible} ${styles.progress}` : styles.progress}/>
                    <Button icon={'send'} label="שליחה" className={this.props.pending ? `${styles.pending} ${styles.submitBtn}` : styles.submitBtn } theme={styles} maxLength={150} onClick={this.handleSubmit} disabled={this.props.pending}/>
                </div>
            </form>
        );
    }
}
