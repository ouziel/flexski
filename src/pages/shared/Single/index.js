import React, {PropTypes as T} from 'react';
import CSSModules from 'react-css-modules'
import styles from './style.scss'
import {Button, IconButton} from 'react-toolbox/lib/button';
import firebase from 'firebase'
import {observer} from 'mobx-react'
import ProgressBar from 'react-toolbox/lib/progress_bar';
import {Card,  CardTitle, CardText, CardActions} from 'react-toolbox/lib/card';
import {withRouter, Link} from 'react-router'
import counterpart from 'counterpart'
import Page from 'pages/shared/Page'
import Avatar from 'react-toolbox/lib/avatar';
import Chip from 'react-toolbox/lib/chip';
import prop from 'appUtil/props'

const t = (label, value) => counterpart(`wizard.${label}.${value}`)
const Field = ({label, value, translate} = {}) => {
    return (
        <div className={styles.row}>
            <span className={styles.label}>{counterpart(`wizard.labels.${label}`)}</span>
            <span className={styles.value}>
                {translate
                    ? t(label, value)
                    : value}
            </span>
        </div>
    )
}

@withRouter
@observer(['ui'])
class Single extends React.Component {
    state = {
        pending: true,
        item: null
    }
    static propTypes = {
      name: T.string,
      subHeader: T.string,
      prevLink: T.string,
      id: T.string,
      fields: T.array,
      msg: T.bool,
    }
    static defaultProps ={
      msg: true,
    }

    componentDidMount() {
        const {id, name} = this.props
        const ref = firebase.database().ref().child(name).child(id)
        ref.once('value').then(value => {
            const item = value.val();
            this.setState({item})
            this.toggle()

        }).catch(err => {
            this.toggle()
        })
    }
    toggle = () => {
        this.setState({
            pending: !this.state.pending
        })
    }

    deleteItem = key => {
      const {name} = this.props
        const updates = {}
        updates[`${name}/${key}`] = null
        firebase.database().ref().update(updates).then(x => this.props.router.replace(`/admin/${name}`)).catch(err => this.props.ui.showError(err.message))

    }
     editItem = id => {
       this.navigate(`${this.props.editLink}/${id}`)
     }
    navigate = id => this.props.router.replace(id);

    render() {
        const {item, pending} = this.state
        const {name, prevLink, subHeader, id} = this.props
        const {user, tel, email, fields, msg} = this.props
        return (
          <Page name={name} prevLink={prevLink} cols>
            {!pending && <h4 className={'sectionTitle'}>{subHeader}</h4>}
            {pending
                ? <div className={'loader'}>
                        <ProgressBar type="circular" mode="indeterminate"/>
                    </div>
                : <Card>
                    <CardTitle className={styles.userInfo}>
                        <span className={styles.user}>
                          <Chip>
                            <Avatar  icon="account_box" />
                            <span>
                              {prop(item,user)}
                            </span>
                          </Chip>

                          </span>
                        <div className={styles.rowUser}>
                            <span className={styles.email}>
                              <Chip>
                                <Avatar  icon="email" />
                          <span>
                            {prop(item,email)}</span>
                        </Chip>
                            </span>
                            <span className={styles.tel}>
                              <Chip>
                                <Avatar  icon="phone" />
                              <span>
                                {prop(item,tel)}
                              </span>
                            </Chip>
                              </span>
                        </div>
                    </CardTitle>
                    <CardText>
                      {!msg && fields && fields.map(field =>
                          <Field key={field.label} label={field.label} value={item[field.parent][field.label]} translate={field.translate ? true : false}/>
                        )}
                      {!fields && msg && <p className={styles.msg}>{item.msg}</p>}
                    </CardText>
                    <CardActions className={styles.footerActions} theme={styles}>
                      {this.props.children}

                        <IconButton icon={'delete'} onClick={() => this.deleteItem(id)}/>
                    </CardActions>
                </Card>
}
          </Page>

        );
    }
}

module.exports = Single