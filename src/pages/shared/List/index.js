import React, {PropTypes as T} from 'react';
import CSSModules from 'react-css-modules'
import styles from './style.scss'
import firebase from 'firebase'
import texts from 'config/texts'
import ProgressBar from 'react-toolbox/lib/progress_bar';
import {withRouter} from 'react-router'
import counterpart from 'counterpart'
import {List, ListItem, ListSubHeader} from 'react-toolbox/lib/list';
import Page from 'pages/shared/Page'



@withRouter
class PageList extends React.Component {
        state = {
            items: [],
            pending: true,
            noPosts: false
        }
        static propTypes = {
          name: T.string,
          subHeader: T.string,
          itemCaption:T.string,
          itemLegend:T.string,
          itemAdminLink: T.bool,
        }
        static defaultProps = {
          itemAdminLink: true
        }
        componentDidMount() {

            this.ref = firebase.database().ref().child(this.props.name)
            this.ref.on('child_added', this.addChild.bind(this))
            this.ref.on('child_removed', this.removeChild.bind(this))
            this.ref.on('child_changed', this.updateChild.bind(this))
            this.checkIfEmpty()
        }

        checkIfEmpty = () => {
            this.timer = setTimeout(() => {
                const {items} = this.state
                if (items.length === 0) {
                    this.setState({pending: false, noPosts: true})
                }
            }, 5000);
        }
        componentWillUnmount() {
            clearTimeout(this.timer)
            this.ref.off()
        }
        removeChild = child => {
            const items = this.state.items.filter(item => item.key !== child.key)
            this.setState({items})
        }
        updateChild = child => {
            const items = this.state.items.map(item => {
                if (item.key === child.key) {
                    item = child
                }
                return item
            })

            this.setState({items})
        }
        addChild = child => {
            let items = this.state.items;
            const item = {
                key: child.key,
                val: child.val()
            }
            items.push(item)
            this.setState({items, item, pending: false})
        }

        navigate = id => {
          const {itemAdminLink,name} = this.props
          let path = `${name}/${id}`
          if (itemAdminLink) {
              path = `/admin/${path}`
          }
          this.props.router.replace(path)
         }
        render() {

            const {items, noPosts, pending} = this.state
            const {subHeader,name, itemCaption, itemLegend} = this.props
            const caption = itemCaption.split('.')
            const legend = itemLegend.split('.')
            return (
                <Page name={name} cols>
                    {!pending && items.length >= 1 &&
                      <List selectable ripple>
                          <ListSubHeader caption={subHeader} className={styles.listHeader} theme={styles}/>
                              {items.map((item, i) => <ListItem className={styles.item} theme={styles} key={i} caption={caption.length > 1 ? item.val[caption[0]][caption[1]] : item.val[caption[0]]} legend={legend.length > 1 ? item.val[legend[0]][legend[1]] : item.val[legend[0]]} onClick={() => this.navigate(item.key)}/>)}
                          </List>
                    }
                    {!pending && noPosts && items.length === 0 && <div className={'center-text'}>{counterpart('app.noPosts')}</div>}
                    {pending && <div className={'loader-box'}>
                        <ProgressBar type="circular" mode="indeterminate"/>
                    </div>}
                </Page>
            );
        }
    }
module.exports = PageList