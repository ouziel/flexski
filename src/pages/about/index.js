import React, {PropTypes as T} from 'react';
import CSSModules from 'react-css-modules'
import styles from './style.scss'
import texts from 'config/texts'
import Page from 'pages/shared/Page'
import Footer from 'pages/shared/Footer'

@CSSModules(styles)
class About extends React.Component {

  render() {
    return (
      <Page name={'about'}>
        <section className={styles.aboutText}>
        <article className={styles.top}>
        <p>
        {texts.pages.about[0]}
        </p>
        <p>
        {texts.pages.about[1]}
        </p>
      </article>
        <article className={styles.main}>
        <p>
        {texts.pages.about[2]}
        </p>
        <p>
        {texts.pages.about[3]}
        </p>
        <p>

        </p>
      </article>
        <article className={styles.bottom}>
        <p>
        {texts.pages.about[4]}
        </p>
      </article>

      </section>
          <div className={styles.bg}/>
      </Page>
      );
  }
}


module.exports = About

