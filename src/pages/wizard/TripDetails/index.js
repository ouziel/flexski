import React, {PropTypes as T} from 'react';
import CSSModules from 'react-css-modules'
import styles from './form.scss'
import {Button, IconButton} from 'react-toolbox/lib/button';
import Input from 'react-toolbox/lib/input';
import DatePicker from 'react-toolbox/lib/date_picker';
import Checkbox from 'react-toolbox/lib/checkbox';
import {RadioGroup, RadioButton} from 'react-toolbox/lib/radio';
import Dropdown from 'react-toolbox/lib/dropdown';
import {withRouter} from 'react-router'
import {observer} from 'mobx-react'
import {countries, resorts} from './data'
import {toJS} from 'mobx'
import validateInputs from 'appUtil/validateInputs'
import Page from 'pages/shared/Page'
import Tooltip from 'react-toolbox/lib/tooltip';
import hebLocale from 'config/hebLocale'
const TooltippedDiv = Tooltip(props => <div {...props}/>)

const TooltipInfo = ({className}) => (<TooltippedDiv className={className} style={{
    display: 'inline-block'
}} tooltip="ממולץ להתגמש בתאריך כדי לעזור לנו ולכם להרכיב את החבילה הזולה ביותר" label=""/>);


const inputs = {
    budget: {
        fields: ['budget'],
        validate(x) {
            return x > 0
        }
    },
    dates: {
        fields: [
            'startDate', 'endDate'
        ],
        validate(x) {
            return (x)
        }
    },
    locations: {
        fields: [
            'country', 'resort'
        ],
        validate(x) {
            return (x)
        }
    },
    transport: {
        fields: ['transport'],
        validate(x) {
            return (x)
        }
    },
    accomndation: {
        fields: ['accomndation'],
        validate(x) {
            return (x)
        }
    },
    people: {
        fields: ['adults'],
        validate(x) {
            return x > 0
        }
    }
}


@withRouter
@CSSModules(styles, {allowMultiple: true})
@observer(['wizard'])
class TripDetails extends React.Component {
    state = {
        resortList: resorts.france,
        country: '',
        budget: '',
        isFlexable: false,
        children: '',
        adults: '',
        accomndation: ''
    };

    handleChange = (name, value) => {
        this.props.wizard.startedTripDetails = true;
        this.setState({
            ...this.state,
            [name]: value
        });

        if (name === 'country') {
          this.setResortList(value)
        }
    };
    setResortList = country => {
        if (country) {
          const resortList = resorts[country]
          this.setState({resortList})
        }
    }
    componentWillReceiveProps(props) {

        this.setState({
            ...props.wizard.tripDetails
        })
    }
    componentDidMount() {
        const {query} = this.props.location

        this.setState({
            ...this.props.wizard.tripDetails
        })

        if (query) {
          this.setState({...query})
          this.setResortList(query.country)
        }
    }

    handleSubmit = (e) => {
        e.preventDefault();
        const {refs} = this
        const shouldUpdateState = validateInputs(this, inputs)
        if (shouldUpdateState) {
            this.updateStore()
        }
    }
    updateStore = () => {
        this.props.wizard.finishedTripDetails = true;
        this.props.wizard.tripDetails = this.state
        this.props.router.replace('/wizard/user')
    }
    scrollTop = () => {
        // document.body.scrollTop = 0;
    }
    render() {

        const {wizard} = this.props;
        const tripDetails = toJS(wizard.tripDetails)
        return (

          <Page name={'wizard.tripDetails'}
            prevLink={'/wizard'}
            nextLink={this.props.wizard.finishedTripDetails ? '/wizard/user' : null}
            innerClass={styles.inner}>
            <section>
                <form className={styles.form} onSubmit={this.handleSubmit}>
                    <div className={styles.row} tabIndex={0} ref={'budget'}>
                        <h5 className={styles.rowTitle}>{'מה התקציב שלכם?'}</h5>
                        <div className={styles.rowInner}>
                            <Input
                                required
                                icon="euro_symbol"
                                type='number'
                                label='תקציב'
                                name='name'
                                value={this.state.budget}
                                onChange={this.handleChange.bind(this, 'budget')}
                                min="0"
                                step={250}
                                className={styles.textInput}
                                theme={styles}/>
                        </div>
                    </div>
                    <div className={styles.row} tabIndex={1} ref={'dates'}>
                        <h5 className={styles.rowTitle}>{'מתי תרצו לנפוש?'}</h5>
                        <div className={styles.rowDates}>
                            <DatePicker
                                sundayFirstDayOfWeek={true}
                                autoOk={true}
                                onClick={this.scrollTop}
                                required
                                icon="date_range"
                                label={'תאריך יציאה'}
                                locale={hebLocale}
                                onChange={this.handleChange.bind(this, 'startDate')}
                                value={this.state.startDate}
                                    className={styles.datePicker}
                                inputClassName={styles.textInput}
                                theme={styles}/>
                            <DatePicker
                                minDate={this.state.startDate}
                                onClick={this.scrollTop}
                                sundayFirstDayOfWeek={true}
                                autoOk={true}
                                required
                                icon="date_range"
                                label={'תאריך חזרה'}
                                className={styles.datePicker}
                                locale={hebLocale}
                                onChange={this.handleChange.bind(this, 'endDate')}
                                value={this.state.endDate}
                                inputClassName={styles.textInput}
                                theme={styles}/>
                            <TooltipInfo className={styles.tooltip}     tooltip='Bookmark Tooltip'
                                tooltipDelay={1000}/>
                            <Checkbox

                                checked={this.state.isFlexable}
                                label={'תאריכים גמישים'}
                                onChange={this.handleChange.bind(this, 'isFlexable')}
                                className={styles.isFlexable}
                                theme={styles}/>
                        </div>
                    </div>
                    <div className={styles.row} tabIndex={2} ref={'accomndation'}>
                        <h5 className={styles.rowTitle}>{'איפה תרצו לישון?'}</h5>
                        <div className={styles.rowInner}>
                            <RadioGroup required name='accomndation' value={this.state.accomndation} onChange={this.handleChange.bind(this, 'accomndation')}>
                                <RadioButton required label={'מלון'} value='hotel' className={styles.radioBtn} theme={styles}/>
                                <RadioButton required label={'דירה'} value='apt' className={styles.radioBtn} theme={styles}/>
                                <RadioButton required label={'אין העדפה'} value='noPrefs' className={styles.radioBtn} theme={styles}/>
                            </RadioGroup>
                        </div>
                    </div>
                    <div className={styles.row} tabIndex={3} ref={'transport'}>
                        <h5 className={styles.rowTitle}>{'כיצד תרצו להגיע לאתר?'}</h5>
                        <div className={styles.rowInner}>
                            <RadioGroup required name='transport' value={this.state.transport} onChange={this.handleChange.bind(this, 'transport')}>
                                <RadioButton required label={'רכב'} value='car' className={styles.radioBtn} theme={styles}/>
                                <RadioButton required label={'הסעה פרטית'} value='privateRide' className={styles.radioBtn} theme={styles}/>
                                <RadioButton required label={'הסעה'} value='publicRide' className={styles.radioBtn} theme={styles}/>
                                <RadioButton required label={'אין העדפה'} value='noPrefs' className={styles.radioBtn} theme={styles}/>
                            </RadioGroup>
                        </div>
                    </div>
                    <div className={styles.row} tabIndex={4} ref={'people'}>
                        <h5 className={styles.rowTitle}>{'כמה אנשים ישנם בקבוצה?'}</h5>
                        <div className={styles.rowInner}>
                            <Input
                                type='number'
                                label='מבוגרים'
                                name='name'
                                min="0"
                                required
                                value={this.state.adults}
                                onChange={this.handleChange.bind(this, 'adults')}
                                className={`${styles.textInput} ${styles.digits}`}
                                theme={styles}/>
                            <Input
                                type='number'
                                label='ילדים'
                                name='name'
                                min="0"
                                required
                                value={this.state.children}
                                onChange={this.handleChange.bind(this, 'children')}
                                className={`${styles.textInput} ${styles.digits}`}
                                theme={styles}/>
                        </div>
                    </div>

                    <div className={styles.row} tabIndex={5} ref={'locations'}>
                        <h5 className={styles.rowTitle}>{'לאן תרצו לטוס?'}</h5>
                        <div className={styles.rowInner}>
                            <div className={styles.cols}>
                                <span className={styles.label}>{'מדינה'}</span>
                                <Dropdown required auto onChange={this.handleChange.bind(this, 'country')} source={countries} value={this.state.country} className={styles.countries} theme={styles}/>
                            </div>
                            <div className={styles.cols}>
                                <span className={styles.label}>{'אתר סקי'}</span>
                                <Dropdown required auto onChange={this.handleChange.bind(this, 'resort')} source={this.state.resortList} className={styles.countries} theme={styles} value={this.state.resort}/>
                            </div>
                        </div>
                    </div>
                    <div className={styles.row}>
                        <div className={styles.saveBox}>
                            <input type="submit" hidden/>
                            <Button label={'שמירה'} className={styles.saveBtn} theme={styles} onClick={this.handleSubmit}/>
                        </div>
                    </div>
                </form>
            </section>
          </Page>

        );
    }
}
module.exports = TripDetails
