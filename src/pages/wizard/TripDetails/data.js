import counterpart from 'counterpart'
let resorts = {
  france: [
    'meribel',
    'courchevel',
    'les 3 valle',
    'les menuires',
    'chamonix',
    'avoriaz',
    "val d'isere",
    "valmorel",
    "la plagne",
    "tignes",
    "La Rosière",
    "morzine",
    "val thorens",
    "les arcs",
    "chatel",
  ],
  italy: [
    "breuil cervinia",
    "val tournenche",
    "santa caterina",
    "Madonna di Campiglio",
    "Val Di Fassa",
    "Val Gardena",
    "sestriere",
    "livigno",
    "sella ronda",
    "passo tonelo",
    "bormio",

  ],
  austria: [
    "solden",
    "ski welt",
    "zell am see",
    "kitzbühel",
    "flachau",
    "bad gastien",
    "lech",
    "alpbach",
    "insbruk",
    "obergurgl",
    "schladming",
    "saalbach",
"ischgl",
"mayrhofen",
"st anton",
  ],
  bulgary: [
    "bansko",
"borovetz",
"pomporovo",
  ],
  andora: [
    "grandvalira", "vallnord",
  ],
  switzerland: [
    "st. moritz",
"zermatt",
"engelberg",
  ],
  chechrepublich: [
    "spindleruv-mlyn"
  ],
  germany: [
    "Garmisch-Partenkirchen-Zugspitze"
  ],
  serbia: [
    "kopaonik",
  ],
  solvakia: [
    "jasna nizke tatry",
  ],
  "spain": [
    "baqueira beret",
  ],
  giorgia: [
    "gudauri"
  ]
}

function capitalize(str) {
return str.charAt(0).toUpperCase() + str.slice(1)
}
function makeDropdownObject(obj) {

const keys = Object.keys(obj)
const data = keys.reduce((acc,cur) => {
    const values = obj[cur]
    acc[cur] = values.map(x => {
      x = { label: capitalize(x), value: x }
      return x;
    })
    acc[cur].push({label: counterpart('app.noPrefs'), value: counterpart('app.noPrefs')})
    return acc;
},{})

return data;
}
resorts = makeDropdownObject(resorts)
export  {resorts}
export const countries = [
  { label: "צרפת", value: 'france' },
  { label: "איטליה", value: 'italy' },
  { label: "אוסטריה", value: 'austria' },
  { label: "בולגריה", value: 'bulgary' },
  { label: "אנדורה", value: 'andora' },
  { label: "שוויץ", value: 'switzerland' },
  { label: "צ׳כיה", value: 'chechrepublich' },
  { label: "גרמניה", value: 'germany' },
  { label: "סרביה", value: 'serbia' },
  { label: "סלובקיה", value: 'solvakia' },
  { label: "ספרד", value: 'spain' },
  { label: "גיאורגיה", value: 'giorgia' },
]

