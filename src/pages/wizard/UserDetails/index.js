import React, {PropTypes as T} from 'react';
import CSSModules from 'react-css-modules'
import styles from './style.scss'
import {Button, IconButton} from 'react-toolbox/lib/button';
import {withRouter, Link} from 'react-router'
import ContatForm from 'pages/shared/contactForm';
import Input from 'react-toolbox/lib/input';
import {observer} from 'mobx-react'
import {toJS} from 'mobx'

import texts from 'config/texts'
import omit from 'object.omit'
import firebase from 'firebase';
import moment from 'moment'
import Page from 'pages/shared/Page'
const inputs = {
    name: {
        fields: ['name'],
        validate(x) {
            return (x)
        }
    },
    tel: {
        fields: ['tel'],
        validate(x) {
            return /\d/.test(x)
        }
    },
    email: {
        fields: ['email'],
        validate(x) {
            return x.includes('@')
        }
    },
    msg: {
        fields: ['msg'],
        validate(x) {
            return true
        }
    },
    approvedTerms: {
        fields: ['approvedTerms'],
        validate(x) {
            return x === true
        }
    }
}


const Thanks = () => {
    return (
        <div className={styles.thanksWrapper}>
            <h4>תודה רבה על פנייתכם.</h4>
            <h4>נציגנו יצרו עמכם קשר בהקדם!</h4>
        </div>
    )
}
@withRouter
@CSSModules(styles)
@observer(['wizard', 'ui'])
class UserDetails extends React.Component {

    state = {
        formActive: false,
        extras: '',
        pending: false,
        name: '',
        tel: '',
        email: '',
        msg: '',
        hint: texts.extrasHint
    };

    leadsRef = firebase.database().ref().child('leads')
    handleChange = (name, value) => {
        this.setState({
            ...this.state,
            [name]: value
        });
        if (name === 'msg' && value) {
            this.setState({hint: ''})
        } else if (name === 'msg' && !value) {
            this.setState({hint: extrasHint})
        }
    };

    componentWillReceiveProps(props) {
        this.setState({
            ...props.wizard.tripDetails
        })
    }
    componentDidMount() {
        this.setState({
            ...this.props.wizard.tripDetails
        })
      if(!this.props.wizard.finishedTripDetails) {
        this.props.router.replace('/wizard/trip')
      }
    }

    toggle = () => {
        this.setState({
            pending: !this.state.pending
        })
    }
    handleSubmit = (formData) => {
        // this.props.wizard.finishedUserDetails = true;
        // this.props.wizard.userDetails = this.state
        this.toggle();
        let user = omit(formData, ['hint'])
        user = { ...user, date: moment().format('DD-MM-YYYY') }
        const trip = omit(toJS(this.props.wizard.tripDetails), ['resortList'])
        trip.startDate = trip.startDate.toDateString();
        trip.endDate = trip.endDate.toDateString();
        trip.children = parseInt(trip.children)
        trip.adults = parseInt(trip.adults)
        trip.budget = parseInt(trip.budget)

        const data = {  user,trip}
        const key = this.leadsRef.push().key
        const updates = {}
        updates[`leads/${key}`] = data
        firebase.database().ref().update(updates).then(res => {
            this.setState({postSuccess: true})
            this.toggle()
            this.props.wizard.reset()
            setTimeout(() => {
              this.props.router.replace('/')
            },3000)
        }).catch(err => {
            this.props.ui.showError(texts.formSubmitError)
            this.toggle()
        })

    }

    updateStore = (name,value) => {
      this.props.wizard.userDetails[name] = value
    }
    render() {
      const userDetails = toJS(this.props.wizard.userDetails)
        return (
          <Page name={'wizard.userDetails'} prevLink={'/wizard/trip'}>
          <div className={styles.row}>
              {/* <h4 className={styles.rowTitle}>{'פרטי התקשרות'}</h4> */}
              <div className={styles.rowInner}>
                  {this.state.postSuccess
                      ? <Thanks/>
                      : <ContatForm handleSubmit={this.handleSubmit.bind(this)} isStep={true} pending={this.state.pending} inputs={inputs} data={userDetails} updateStore={this.updateStore.bind(this)}/>}
              </div>
          </div>
          </Page>

        )
    }
}
module.exports = UserDetails

