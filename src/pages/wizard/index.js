import React, {PropTypes as T} from 'react';
import {toJS} from 'mobx'
import {observer} from 'mobx-react'
import CSSModules from 'react-css-modules'
import styles from './style.scss'
import {Button, IconButton} from 'react-toolbox/lib/button';
import {withRouter, Link} from 'react-router'
import Page from 'pages/shared/Page'
import Footer from 'pages/shared/Footer'
import counterpart from 'counterpart'
const data = [
    {
        title: 'מילוי פרטים ובחירת אפשרויות',
        icon: require('assets/svg/1.svg'),
        text: 'מסירת פרטים אישיים לצורך המשך התקשרות ובחירת העדפות נסיעה כמו תאריכים, תקציב, יעדים מועדפים וכו׳'
    }, {
        title: 'קבלת הצעת מחיר',
        icon: require('assets/svg/2.svg'),
        text: 'לאחר קבלת פנייתכם ובתום עיבוד הפנייה על ידי סוכן, נשלח לכם לינק להצעת מחיר לכתובת הדוא״ל שסיפקתם'
    }, {
        title: 'השלמת פרטי דרכון',
        icon: require('assets/svg/3.svg'),
        text: "מילוי פרטי דרכון ופרטי נסיעה נוספים עבור כל חברי הקבוצה"
    }
]
@withRouter
@CSSModules(styles)
@observer(['wizard'])
class Wizard extends React.Component {
    state = {
        formActive: false
    }
    openForm = () => {
        this.setState({formActive: true})
    }

    navigate = id => {
        this.props.router.replace(id)
    }
    render() {
        return (
            <Page name={'wizard'} nextLink={this.props.wizard.startedTripDetails
                ? '/wizard/trip'
                : null} innerClass={styles.inner}>
                ֿ<h1 className={styles.innerTitle}>{counterpart('wizard.explainProccess')}</h1>
                <ul className={styles.list}>
                    {data.map((item, i) => <li className={styles.item} key={i}>
                        <img src={item.icon}/>
                        <h2>{item.title}</h2>
                        <p>{item.text}</p>
                    </li>)}
                </ul>
                <Link to={'/wizard/trip'}>
                    <Button label={counterpart('wizard.ctaText')} className={styles.startButton} theme={styles} onClick={() => this.openForm()}/>
                </Link>
                <div className={styles.bgRight}/>
            </Page>
        )

    }
}

module.exports = Wizard
