import React, {PropTypes as T} from 'react';
import counterpart from 'counterpart'
import PageList from 'pages/shared/List'
class GroupList extends React.Component {

    render() {

        return (

          <PageList
            name={'groups'}
            subHeader={counterpart('groups.messages')}
            itemCaption='package.country'
            itemLegend='package.resort'
            itemAdminLink={false}
            />

        );
    }
}

module.exports = GroupList