import React, {PropTypes as T} from 'react';
import CSSModules from 'react-css-modules'
import styles from './style.scss'
import {IconButton} from 'react-toolbox/lib/button';
import ProgressBar from 'react-toolbox/lib/progress_bar';
import {Tab, Tabs} from 'react-toolbox';
import {Link, withRouter} from 'react-router'
import {observer} from 'mobx-react'
import {toJS} from 'mobx'
import validateInputs from 'appUtil/validateInputs'
import counterpart from 'counterpart'
import {Card, CardText, CardActions} from 'react-toolbox/lib/card';
import Package from './Package'
import Passangers from './Passangers'
import Page from 'pages/shared/Page'

@withRouter
@CSSModules(styles, {allowMultiple: true})
@observer(['session', 'ui'])
class SingleGroup extends React.Component {
    state = {
        data: null,
        tabIndex: 0
    }

    componentDidMount() {

        this.getData()

    }
componentWillUnmount() {
  this.ref.off()
}
getData = () => {
  
  const {id} = this.props.params
  this.ref = firebase.database().ref().child(`groups/${id}`)
  this.ref.on('value', d => this.setState({data: d.val()}))
}
    handleTabChange = tabIndex => this.setState({tabIndex})
    deleteItem = key => {
        const updates = {}
        updates[`groups/${key}`] = null
        firebase.database().ref().update(updates).then(x => this.props.router.replace('/admin/groups')).catch(err => this.props.ui.showError(err.message))

    }
    render() {

        const {id} = this.props.params
        const {data} = this.state
        const {session} = this.props
        const isAdmin = toJS(session.isAdmin)

        return (

          <Page name={'groupSingle'} prevLink={isAdmin ? '/admin/groups' : null} cols>
            {data && <div className={styles.intro}>
                {counterpart('groups.packageReady')}
            </div>}
            {data
                ? <Card className={styles.pageCard} theme={styles}>

                        <CardText>
                            <Tabs fixed={true} index={this.state.tabIndex} onChange={this.handleTabChange} className={styles.pageTabs} theme={styles}>
                                <Tab label={counterpart('groups.package')} className={styles.tab} theme={styles}><Package data={data}/></Tab>
                                <Tab label={counterpart('groups.members')} className={styles.tab} theme={styles}><Passangers data={data} id={id} getData={this.getData}/></Tab>
                            </Tabs>
                        </CardText>
                        {isAdmin && <CardActions className={styles.footerActions} theme={styles}>
                            <IconButton icon={'delete'} onClick={() => this.deleteItem(id)}/>
                            <IconButton icon={'edit'} onClick={() => this.props.router.replace(`/admin/quote/${id}`)}/>
                        </CardActions>}
                    </Card>
                : <div className={'loader-box'}>
                    <ProgressBar type="circular" mode="indeterminate"/>
                </div>}
          </Page>
        );
    }
}
module.exports = SingleGroup
