import React, {PropTypes as T} from 'react';
import CSSModules from 'react-css-modules'
import styles from './style.scss'
import {Button} from 'react-toolbox/lib/button';
import Input from 'react-toolbox/lib/input';
import DatePicker from 'react-toolbox/lib/date_picker';
import Checkbox from 'react-toolbox/lib/checkbox';
import {RadioGroup, RadioButton} from 'react-toolbox/lib/radio';
import {Link, withRouter} from 'react-router'
import {observer} from 'mobx-react'
import {countries, resorts} from 'pages/wizard/TripDetails/data'
import {toJS} from 'mobx'
import omit from 'object.omit'
import validateInputs from 'appUtil/validateInputs'
import counterpart from 'counterpart'
import hebLocale from 'config/hebLocale'
// import TimePicker from 'react-toolbox/lib/time_picker';

const LicenceLabel = () => {
  return (
    <div className={styles.terms}>
      <span>{counterpart('app.approveTerms')}</span>
      <Link to={'/terms'}>{counterpart('app.terms')}</Link>
    </div>
  )
}
const inputs = {
    name: {
        fields: ['name'],
        validate(x) {
            return (x)
        }
    },
    tel: {
        fields: ['tel'],
        validate(x) {
            return (x)
        }
    },
    email: {
        fields: ['email'],
        validate(x) {
            return (x)
        }
    },
    gender: {
        fields: ['gender'],
        validate(x) {
            return x !== null
        }
    },
    passportExpairy: {
        fields: ['passportExpairy'],
        validate(x) {
            return (x)
        }
    },
    passportBirth: {
        fields: ['passportBirth'],
        validate(x) {
            return (x)
        }
    },
    passportNum: {
        fields: ['passportNum'],
        validate(x) {
            return (x)
        }
    },
    passportName: {
        fields: ['passportName'],
        validate(x) {
            return (x)
        }
    },
    birthday: {
        fields: ['birthday'],
        validate(x) {
            return (x)
        }
    },
    needsSkiGear: {
        fields: ['needsSkiGear'],
        validate(x) {
            return x !== null
        }
    },
    approvedTerms: {
        fields: ['approvedTerms'],
        validate(x) {
            return x === true
        }
    }
}

const dateToStr = value => value instanceof Date
    ? value.toDateString()
    : value

@withRouter
@CSSModules(styles, {allowMultiple: true})
@observer(['wizard', 'ui'])
class PassangerForm extends React.Component {
    state = {
        email: '',
        gender: ''
    };

    componentDidMount() {
        const {data} = this.props;
        this.setState({
            ...data
        })
    }
    componentWillReceiveProps(props) {
        const {data} = props;
        this.setState({
            ...data
        })
    }
    handleChange = (name, value) => {
        this.setState({
            ...this.state,
            [name]: value
        });
        if (name === 'country') {
            const resortList = resorts[value]
            this.setState({resortList})
        }

    };

    handleSubmit = (e) => {
        e.preventDefault();
        const {refs} = this
        const shouldUpdateState = validateInputs(this, inputs)
        if (shouldUpdateState) {
            this.update(this.state)
        }
    }
    update = rawData => {
        const {memberKey, groupID} = this.props
        let data = {
            ...omit(rawData, ['passportName', 'passportNum', 'passportExpairy', 'passportBirth']),
            passportName: dateToStr(rawData.passportName),
            passportNum: dateToStr(rawData.passportNum),
            passportExpairy: dateToStr(rawData.passportExpairy),
            passportBirth: dateToStr(rawData.passportBirth)
        }
        const ref = firebase.database().ref().child(`groups/${groupID}/${memberKey}`)
        const updates = {}
        updates[`groups/${groupID}/members/${memberKey}`] = data
        firebase.database().ref().update(updates).then(x => {
          this.props.toggleForm()
        }
      ).catch(err => {
        console.log(err)
        this.props.ui.showErr(err.message)
      })

    }

    render() {

        return (

            <form className={styles.form} onSubmit={this.handleSubmit}>
                <div className={styles.row}>
                    <h5 className={'sectionTitle'}>{counterpart('passanger.contacts')}</h5>
                    <div className={styles.row} tabIndex={0}>
                        <div className={styles.rowInner} ref={'name'}>
                            <Input type='text' label={counterpart('passanger.name')} name='name' value={this.state.name} onChange={this.handleChange.bind(this, 'name')} className={styles.formInput} theme={styles}/>
                        </div>
                    </div>

                    <div className={styles.row}>
                        <div className={styles.rowInner} tabIndex={1} ref={'tel'}>
                            <Input type='tel' label={counterpart('passanger.tel')} name='tel' value={this.state.tel} onChange={this.handleChange.bind(this, 'tel')} className={styles.formInput} theme={styles}/>
                        </div>
                    </div>

                    <div className={styles.row}>
                        <div className={styles.rowInner} tabIndex={2} ref={'email'}>
                            <Input type='email' label={counterpart('passanger.email')} name='email' value={this.state.email} onChange={this.handleChange.bind(this, 'email')} className={styles.formInput} theme={styles}/>
                        </div>
                    </div>
                </div>

                <div className={styles.row}>
                    <h5 className={'sectionTitle'}>{counterpart('passanger.passportDetails')}</h5>
                    <div className={styles.row}>
                        <div className={styles.rowInner} tabIndex={3} ref={'passportExpairy'}>
                            <DatePicker locale={hebLocale} className={styles.datePicker} inputClassName={styles.formInput} label={counterpart('passanger.passportExpairy')} sundayFirstDayOfWeek autoOk={true} onChange={this.handleChange.bind(this, 'passportExpairy')} value={this.state.passportExpairy}/>
                        </div>
                    </div>

                    <div className={styles.row}>
                        <div className={styles.rowInner} tabIndex={4} ref={'passportBirth'}>

                            <DatePicker className={styles.datePicker} inputClassName={styles.formInput} locale={hebLocale} label={counterpart('passanger.passportBirth')} sundayFirstDayOfWeek autoOk={true} onChange={this.handleChange.bind(this, 'passportBirth')} value={this.state.passportBirth}/>
                        </div>
                    </div>

                    <div className={styles.row}>
                        <div className={styles.rowInner} tabIndex={5} ref={'passportNum'}>
                            <Input type='text' label={counterpart('passanger.passportNum')} name='passportNum' value={this.state.passportNum} onChange={this.handleChange.bind(this, 'passportNum')}/>
                        </div>

                    </div>
                    <div className={styles.row}>
                        <div className={styles.rowInner} tabIndex={6} ref={'passportName'}>
                            <Input type='text' label={counterpart('passanger.passportName')} name='passportName' value={this.state.passportName} onChange={this.handleChange.bind(this, 'passportName')} className={styles.formInput} theme={styles}/>
                        </div>

                    </div>
                </div>

                <div className={styles.row}>
                    <h5 className={'sectionTitle'}>{counterpart('passanger.general')}</h5>

                    <div className={styles.row}>
                        <div className={styles.rowInner} tabIndex={7} ref={'birthday'}>
                            <DatePicker label={counterpart('passanger.birthday')} sundayFirstDayOfWeek autoOk={true} onChange={this.handleChange.bind(this, 'birthday')} value={this.state.birthday} className={styles.datePicker} inputClassName={styles.formInput} locale={hebLocale}/>
                        </div>
                    </div>



                    <div className={styles.row}>
                        <div className={styles.rowInner} tabIndex={8} ref={'gender'}>
                            <RadioGroup name={counterpart('passanger.gender')} value={this.state.gender} onChange={value => this.handleChange('gender', value)}>
                                <RadioButton label={counterpart('passanger.labels.gender.male')} value='male'/>
                                <RadioButton label={counterpart('passanger.labels.gender.female')} value='female'/>
                            </RadioGroup>
                        </div>
                    </div>

                    <div className={styles.row}>
                        <div className={styles.rowInner}>

                          <Checkbox
                              checked={this.state.needsSkiGear}
                              label={counterpart('passanger.needsSkiGear')}
                              onChange={this.handleChange.bind(this, 'needsSkiGear')}
                              className={styles.needsSkiGear}
                              theme={styles}/>
                        </div>
                    </div>


                    <div className={styles.row}>
                        <div className={styles.rowInner} tabIndex={8} ref={'approvedTerms'}>
                                      <Checkbox
                                          ref={'approvedTerms'}
                                          checked={this.state.approvedTerms}
                                          label={<LicenceLabel/>}
                                          onChange={this.handleChange.bind(this, 'approvedTerms')}
                                          className={styles.needsSkiGear}
                                          theme={styles}/>
                    </div>
                  </div>

                </div>

                <div className={styles.row}>
                    <div className={'flex-center'}>
                        <input type="submit" hidden/>
                        <Button label={'שמירה'} className={styles.button} theme={styles} onClick={this.handleSubmit}/>
                    </div>
                </div>
            </form>

        );
    }
}
module.exports = PassangerForm
