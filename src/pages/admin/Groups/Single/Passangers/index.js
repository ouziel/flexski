import React, {PropTypes as T} from 'react';
import CSSModules from 'react-css-modules'
import styles from './style.scss'
import {Button, IconButton} from 'react-toolbox/lib/button';
import Input from 'react-toolbox/lib/input';
import DatePicker from 'react-toolbox/lib/date_picker';
import Checkbox from 'react-toolbox/lib/checkbox';
import {RadioGroup, RadioButton} from 'react-toolbox/lib/radio';
import Dropdown from 'react-toolbox/lib/dropdown';
import {Link, withRouter} from 'react-router'
import {observer} from 'mobx-react'
import {countries, resorts} from 'pages/wizard/TripDetails/data'
import {toJS} from 'mobx'
import omit from 'object.omit'
import moment from 'moment'
import validateInputs from 'appUtil/validateInputs'
import {List, ListItem, ListSubHeader, ListDivider, ListCheckbox} from 'react-toolbox/lib/list';
import hebLocale from 'config/hebLocale'
import Form from './form'
import counterpart from 'counterpart'
// import TimePicker from 'react-toolbox/lib/time_picker';
let time = new Date();
time.setHours(17);
time.setMinutes(28);

const inputs = {
    price: {
        fields: ['price'],
        validate(x) {
            return x > 0
        }
    },
    dates: {
        fields: [
            'startDate', 'endDate'
        ],
        validate(x) {
            return (x)
        }
    },
    locations: {
        fields: [
            'country', 'resort'
        ],
        validate(x) {
            return (x)
        }
    },
    transport: {
        fields: ['transport'],
        validate(x) {
            return (x)
        }
    },
    accomndation: {
        fields: ['accomndation'],
        validate(x) {
            return (x)
        }
    },
    people: {
        fields: ['people'],
        validate(x) {
            return x > 0
        }
    }
}

@withRouter
@CSSModules(styles, {allowMultiple: true})
@observer(['wizard'])
class PassangersList extends React.Component {

    state = {
        members: {},
        showForm: false,
        memberKey: ''
    }
    componentDidMount() {
        const {data} = this.props;
        if (data && data.members) {
            this.setState({members: data.members})
        }
    }
    toggleForm = () => {
        this.setState({
            showForm: !this.state.showForm
        })
        // this.props.getData()
    }
    showUserForm = memberKey => {
        this.toggleForm();
        this.setState({memberKey})

    }
    render() {
        const keys = Object.keys(this.state.members)
        const {members, showForm, memberKey} = this.state
        return (
            <div>
                {showForm && <Form toggleForm={this.toggleForm.bind(this)} memberKey={memberKey} groupID={this.props.id} data={members[memberKey]}/>}
                {!showForm && <List selectable ripple>
                    {keys.map(key => <ListItem key={key} onClick={() => this.showUserForm(key)} caption={members[key].name} legend={members[key].email}/>)
}
                </List>}

            </div>

        );
    }
}
module.exports = PassangersList
