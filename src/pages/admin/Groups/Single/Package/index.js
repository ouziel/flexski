import React, {PropTypes as T} from 'react';
import CSSModules from 'react-css-modules'
import styles from './style.scss'
import {countries, resorts} from 'pages/wizard/TripDetails/data'
import counterpart from 'counterpart'
import DatesIcon from 'components/Svgs/dates'
import FlightsIcon from 'components/Svgs/flights'
import HotelIcon from 'components/Svgs/hotel'
import LocationIcon from 'components/Svgs/locations'
import TransportIcon from 'components/Svgs/transport'
import {Tab, Tabs} from 'react-toolbox'
import FontIcon from 'react-toolbox/lib/font_icon';
import moment from 'moment'

class Package extends React.Component {
    state = {
        tabIndex: 0
    }
    handleTabChange = tabIndex => this.setState({tabIndex})
    render() {
        const {data} = this.props
      
        return (
            <ul className={styles.grid}>
                {/* flights */}
                <li className={styles.item}>

                    <FlightsIcon svgClass={styles.svg} circleClass={styles.FlightsCircle}/>
                    <h4 className={styles.itemTitle}>{counterpart('groups.labels.flights')}</h4>
                    <Tabs fixed={true} inverse={false} index={this.state.tabIndex} onChange={this.handleTabChange} className={styles.tabs}>
                        <Tab label={counterpart('groups.labels.flightTo')}>
                            <div className={styles.subrow}>
                                <span className={styles.label}>
                                    {counterpart('wizard.labels.startDate')}
                                </span>
                                <span className={styles.value}>
                                  {moment(data.package.startDate).format('DD-MM-YYYY')}
                                </span>
                            </div>
                            <div className={styles.subrow}>
                                <span className={styles.label}>
                                    {counterpart('groups.labels.departure')}
                                </span>
                                <span className={styles.value}>
                                      {moment(data.package.startDepartureTime).format('HH:mm')}
                                </span>
                            </div>
                            <div className={styles.subrow}>
                                <span className={styles.label}>
                                    {counterpart('groups.labels.landing')}
                                </span>
                                <span className={styles.value}>
                                {moment(data.package.startLandingTime).format('HH:mm')}
                                </span>
                            </div>
                        </Tab>
                        <Tab label={counterpart('groups.labels.flightFrom')}>
                            <div className={styles.subrow}>
                                <span className={styles.label}>
                                    {counterpart('wizard.labels.endDate')}
                                </span>
                                <span className={styles.value}>
                                  {moment(data.package.endDate).format('DD-MM-YYYY')}
                                </span>
                            </div>
                            <div className={styles.subrow}>
                                <span className={styles.label}>
                                    {counterpart('groups.labels.departure')}
                                </span>
                                <span className={styles.value}>
                                  {moment(data.package.endDepartureTime).format('HH:mm')}
                                </span>
                            </div>
                            <div className={styles.subrow}>
                                <span className={styles.label}>
                                    {counterpart('groups.labels.landing')}
                                </span>
                                <span className={styles.value}>
                                  {moment(data.package.endLandingTime).format('HH:mm')}
                                    {/* {data.package.endDate} */}
                                </span>
                            </div>

                        </Tab>
                    </Tabs>

                </li>

                {/* transport */}
                <li className={styles.item}>

                    <TransportIcon svgClass={styles.svg} circleClass={styles.TransportCircle}/>
                    <h4 className={styles.itemTitle}>{counterpart('wizard.labels.transport')}</h4>
                    <div className={styles.subrow}>
                        <p className={styles.paragraph}>
                            {data.package.transport}
                        </p>
                    </div>

                </li>

                {/* accomndation */}
                <li className={styles.item}>

                    <HotelIcon svgClass={styles.svg} circleClass={styles.HotelCircle}/>
                    <h4 className={styles.itemTitle}>{counterpart('wizard.labels.accomndation')}</h4>
                    <div className={styles.subrow}>
                        <p className={styles.paragraph}>
                            {data.package.accomndation}
                        </p>
                    </div>

                </li>

                {/* resoirt */}
                <li className={styles.item}>

                    <LocationIcon svgClass={styles.svg} circleClass={styles.LocationCircle}/>
                    <h4 className={styles.itemTitle}>{counterpart('groups.labels.location')}</h4>
                    <div>
                        <div className={styles.subrow}>
                            <span className={styles.label}>
                                {counterpart('groups.labels.country')}
                            </span>
                            <span className={styles.value}>
                                {data.package.country}
                            </span>
                        </div>
                        <div className={styles.subrow}>
                            <span className={styles.label}>
                                {counterpart('groups.labels.resort')}
                            </span>
                            <span className={styles.value}>
                                {data.package.resort}
                            </span>
                        </div>
                    </div>

                </li>

                {/* price */}
                <li className={styles.item}>

                    <h4 className={styles.itemTitle}>{counterpart('quote.labels.price')}</h4>
                    <div className={styles.subrow}>
                        <span className={styles.label}>
                            <FontIcon value='euro_symbol'/>
                        </span>
                        <span className={styles.value}>
                            {data.package.price}
                        </span>
                    </div>

                </li>


                {/* extra */}
                <li className={styles.item}>
                    <h4 className={styles.itemTitle}>{counterpart('quote.labels.extras')}</h4>
                      <div className={styles.subrow}>
                          <p className={styles.paragraph}>
                              {data.package.extras}
                          </p>
                      </div>

                </li>


            </ul>
        )
    }
}
module.exports = Package