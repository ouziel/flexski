import React, {PropTypes as T} from 'react';
import CSSModules from 'react-css-modules'
import {observer} from 'mobx-react'
import {Button} from 'react-toolbox/lib/button';
import {withRouter} from 'react-router'
import Single from 'pages/shared/Single'
import counterpart from 'counterpart'

const fields = [
  {
    label: 'budget', parent: 'trip'
  },
  {
    label: 'accomndation', parent: 'trip', translate: true,
  },
  {
    label: 'transport', parent: 'trip', translate: true,
  },
  {
    label: 'startDate', parent: 'trip'
  },
  {
    label: 'endDate', parent: 'trip'
  },
  {
    label: 'isFlexable', parent: 'trip', translate: true,
  },
  {
    label: 'country', parent: 'trip'
  },
  {
    label: 'resort', parent: 'trip'
  },
  {
    label: 'children', parent: 'trip'
  },
  {
    label: 'adults', parent: 'trip'
  },
  {
    label: 'msg', parent: 'user',
  }
]

@withRouter
@observer(['session', 'ui'])
class LeadsSingle extends React.Component {

    render() {
        const {id} = this.props.params

        return (

          <Single
            name={'leads' }
            prevLink={'/admin/leads'}
            id={id}
            subHeader={counterpart('leads.singleLead')}
            user='user.name'
            email='user.email'
            tel='user.tel'
            fields={fields}
            msg={false}
          >
            <Button label={counterpart('wizard.labels.makeQuote')} onClick={() => this.props.router.replace(`/admin/quote/${id}`)}/>
          </Single>

        );
    }
}



module.exports = LeadsSingle