import React, {PropTypes as T} from 'react';
import CSSModules from 'react-css-modules'
import styles from './style.scss'
import {Button, IconButton} from 'react-toolbox/lib/button';
import Input from 'react-toolbox/lib/input';
import DatePicker from 'react-toolbox/lib/date_picker';
import Dropdown from 'react-toolbox/lib/dropdown';
import {Link, withRouter} from 'react-router'
import {observer} from 'mobx-react'
import {countries, resorts} from 'pages/wizard/TripDetails/data'
import omit from 'object.omit'
import validateInputs from 'appUtil/validateInputs'
import counterpart from 'counterpart'
import Page from 'pages/shared/Page'
import firebase from 'firebase'
import hebLocale from 'config/hebLocale'
import TimePicker from 'react-toolbox/lib/time_picker';


const inputs = {
    extras: {
        fields: ['extras'],
        validate(x) {
            return true
        }
    },
    price: {
        fields: ['price'],
        validate(x) {
            return x > 0
        }
    },
    dates: {
        fields: [
            'startDate',
            'endDate',
            'startDepartureTime',
            'startLandingTime',
            'endLandingTime',
            'endDepartureTime'
        ],
        validate(x) {
            return (x)
        }
    },
    locations: {
        fields: [
            'country', 'resort'
        ],
        validate(x) {
            return (x)
        }
    },
    transport: {
        fields: ['transport'],
        validate(x) {
            return (x)
        }
    },
    accomndation: {
        fields: ['accomndation'],
        validate(x) {
            return (x)
        }
    },
    people: {
        fields: ['people'],
        validate(x) {
            return x > 0
        }
    }
}

/* matches firebase Passanger rules in rules.bolt */
const demoUser = {
    name: counterpart('passanger.name'),
    tel: counterpart('passanger.tel'),
    email: counterpart('passanger.email'),
    passportExpairy: new Date().toDateString(),
    passportBirth: new Date().toDateString(),
    passportNum: 1212121,
    passportName: 'Full Name',
    birthday: new Date().toDateString(),
    needsSkiGear: false,
    approvedTerms: false
}

@withRouter
@CSSModules(styles, {allowMultiple: true})
@observer(['ui'])
class Quote extends React.Component {
    state = {
        price: 0,
        people: 0,
        startDate: new Date(),
        endDate: new Date(),
        resortList: resorts.france,
        country: 'france',
        accomndation: '',
        transport: '',
        pending: false
    };
    componentDidMount() {
        /*
          get data if it's available
        */
        const {id} = this.props.params
        this.ref = firebase.database().ref().child(`groups/${id}`)
        this.ref.on('value', this.setData.bind(this))

    }
    componentWillUnmount() {
        this.ref.off()
    }


    handleChange = (name, value) => {
        this.setState({
            ...this.state,
            [name]: value
        });
        if (name === 'country') {
            const resortList = resorts[value]
            this.setState({resortList})
        }

    };

    setData = data => {
        /*
      if this quote's ID exists, fetch it's data
      and set state with needed adjusements
      */
        const val = data.val()
        if (val) {
            this.setState({
                ...omit(val.package, [
                    'startDate',
                    'endDate',
                    'startDepartureTime',
                    'startLandingTime',
                    'endDepartureTime',
                    'endLandingTime'
                ]),
                members: val.members,
                startDate: new Date(val.package.startDate),
                startDepartureTime: new Date(val.package.startDepartureTime),
                startLandingTime: new Date(val.package.startLandingTime),
                endDate: new Date(val.package.endDate),
                endDepartureTime: new Date(val.package.endDepartureTime),
                endLandingTime: new Date(val.package.endLandingTime),
                resortList: resorts[val.package.country],
                resort: resorts[val.package.country].find(x => val.package.resort).value
            })
        }
    }
    toggle = () => {
        this.setState({
            pending: !this.state.pending
        })
    }
    handleSubmit = (e) => {
        /*
     toggle pending state to disable re-sending
     */
        this.toggle();
        e.preventDefault();
        const {refs} = this
        const shouldUpdateState = validateInputs(this, inputs)
        if (shouldUpdateState) {
            /*
          update quote in database
           */
            this.update({
                ...omit(this.state, ['resortList', 'members', 'pending'])
            })

        } else {
            /*
            validation failed, toggle pending back to normal
            */
            this.setState({pending: false})
        }
    }

    update = quote => {
        const {id} = this.props.params
        const {members} = this.state
        const ref = firebase.database().ref().child('groups')
        const data = {
            package: quote
        }
        const updates = {}
        updates[`groups/${id}`] = data
        firebase.database().ref().update(updates).then(() => this.updateMembers({members, quote, id})).catch(x => {
            this.setState({pending: false})
            this.props.ui.showError(x.message)
        })
    }
    updateMembers = ({members, id, quote}) => {
        /*
      set/update members
       */

        if (!members) {
            /*
          not editing, this a new quote
           */
            this.addMembers(quote.people)
        } else {
            /*
           if editing an exsiting quote
          add people according to what's set in state
         */
            const {people} = this.state
            const updates = {}
            updates[`groups/${id}/members`] = members
            firebase.database().ref().update(updates).then(() => {
                let extraPeople = parseFloat(people) - Object.keys(members).length
                if (extraPeople >= 1) {
                    this.addMembers(extraPeople).then(() => {
                        this.props.router.replace(`groups/${id}`)
                    })
                } else {
                    this.props.router.replace(`groups/${id}`)
                }
            }).catch(err => {
                this.toggle()
                this.props.ui.showError(err)
            })
        }
    }

    addMembers = length => {
        /*
      util to add new members with push keys
      and navigate to group page on success
      show error on fail
     */
        const {id} = this.props.params;
        const ref = firebase.database().ref().child('groups');
        const refMembers = ref.child(id).child('members');

        let membersUpdates = [],
            updates = {}

        for (let i = 0; i < length; i++) {
            const memberID = refMembers.push().key
            updates[`groups/${id}/members/${memberID}`] = demoUser
            membersUpdates.push(firebase.database().ref().update(updates))
        }
        return Promise.all(membersUpdates).then(x => {
            this.props.router.replace(`groups/${id}`)
        }).catch(err => {
            this.toggle()
            this.props.ui.showError(err.message)
        })
    }
    render() {

        const {id} = this.props.params
        const {noLead} = this.props.location.query

        return (

            <Page name={'quote'} prevLink={noLead
                ? `/admin/posts/${id}`
                : `/admin/leads/${id}`}>
                <div>
                    <h4 className={'sectionTitle'}>{counterpart('quote.makeOffer')}</h4>
                    <form className={styles.form} onSubmit={this.handleSubmit}>
                        <div className={styles.row} tabIndex={0} ref={'price'}>
                            <h5 className={styles.rowTitle}>{counterpart('quote.labels.price')}</h5>
                            <div className={styles.rowInner}>
                                <Input required icon="euro_symbol" type='number' label={counterpart('quote.labels.price')} name='name' value={this.state.price} onChange={this.handleChange.bind(this, 'price')} min="0" step={250} className={styles.textInput} theme={styles}/>
                            </div>
                        </div>
                        <div className={styles.row} tabIndex={1} ref={'dates'}>
                            <h5 className={styles.rowTitle}>{counterpart('wizard.labels.dates')}</h5>
                            <div className={styles.rowInner}>
                                <div className={styles.subrow}>
                                    <DatePicker sundayFirstDayOfWeek={true} autoOk={true} onClick={this.scrollTop} required icon="date_range" label={counterpart('wizard.labels.startDate')} locale={hebLocale} onChange={this.handleChange.bind(this, 'startDate')} value={this.state.startDate} className={styles.datePicker} inputClassName={styles.textInput} theme={styles}/>

                                    <TimePicker icon="access_time" label={counterpart('wizard.departureTime')} onChange={this.handleChange.bind(this, 'startDepartureTime')} value={this.state.startDepartureTime} className={styles.textInput} theme={styles}/>
                                    <TimePicker icon="access_time" label={counterpart('wizard.landingTime')} onChange={this.handleChange.bind(this, 'startLandingTime')} value={this.state.startLandingTime} className={styles.textInput} theme={styles}/>
                                </div>

                                <div className={styles.subrow}>
                                    <DatePicker minDate={this.state.startDate} onClick={this.scrollTop} sundayFirstDayOfWeek={true} autoOk={true} required icon="date_range" label={counterpart('wizard.labels.endDate')} className={styles.datePicker} locale={hebLocale} onChange={this.handleChange.bind(this, 'endDate')} value={this.state.endDate} inputClassName={styles.textInput} theme={styles}/>
                                    <TimePicker icon="access_time" label={counterpart('wizard.departureTime')} onChange={this.handleChange.bind(this, 'endDepartureTime')} value={this.state.endDepartureTime} className={styles.textInput} theme={styles}/>
                                    <TimePicker icon="access_time" label={counterpart('wizard.landingTime')} onChange={this.handleChange.bind(this, 'endLandingTime')} value={this.state.endLandingTime} className={styles.textInput} theme={styles}/>
                                </div>

                            </div>
                        </div>
                        <div className={styles.row} tabIndex={2} ref={'accomndation'}>
                            <h5 className={styles.rowTitle}>{counterpart('wizard.labels.accomndation')}</h5>
                            <div className={styles.rowInner}>
                                <Input multiline required type='text' label={counterpart('wizard.labels.accomndation')} name='name' icon={'home'} value={this.state.accomndation} onChange={this.handleChange.bind(this, 'accomndation')} className={styles.textInput} theme={styles}/>
                            </div>
                        </div>

                        <div className={styles.row} tabIndex={3} ref={'transport'}>
                            <h5 className={styles.rowTitle}>{counterpart('wizard.labels.transport')}</h5>
                            <div className={styles.rowInner}>
                                <Input multiline required type='text' label={counterpart('wizard.labels.transport')} name='name' icon={'directions_car'} value={this.state.transport} onChange={this.handleChange.bind(this, 'transport')} className={styles.textInput} theme={styles}/>
                            </div>
                        </div>
                        <div className={styles.row} tabIndex={4} ref={'people'}>
                            <h5 className={styles.rowTitle}>{counterpart('wizard.labels.people')}</h5>
                            <div className={styles.rowInner}>
                                <Input type='number' icon={'people'} label={counterpart('wizard.labels.people')} name='name' min="0" required value={this.state.people} onChange={this.handleChange.bind(this, 'people')} className={`${styles.textInput} ${styles.digits}`} theme={styles}/>
                            </div>
                        </div>

                        <div className={styles.row} tabIndex={5} ref={'locations'}>
                            <h5 className={styles.rowTitle}>{counterpart('wizard.labels.destination')}</h5>
                            <div className={styles.rowInner}>
                                <div className={styles.cols}>
                                    <span className={styles.label}>{counterpart('wizard.labels.country')}</span>
                                    <Dropdown required auto icon='location_on' onChange={this.handleChange.bind(this, 'country')} source={countries} value={this.state.country} className={styles.countries} theme={styles}/>
                                </div>
                                <div className={styles.cols}>
                                    <span className={styles.label}>{counterpart('wizard.labels.resort')}</span>
                                    <Dropdown required auto icon='location_on' onChange={this.handleChange.bind(this, 'resort')} source={this.state.resortList} className={styles.countries} theme={styles} value={this.state.resort}/>
                                </div>
                            </div>
                        </div>
                        <div className={styles.row} tabIndex={6} ref={'extras'}>
                            <h5 className={styles.rowTitle}>{counterpart('quote.labels.extras')}</h5>
                            <div className={styles.rowInner}>
                                <Input type='text' label={counterpart('quote.labels.extras')} name='extras' value={this.state.extras} onChange={this.handleChange.bind(this, 'extras')} multiline={true} className={styles.textInput} theme={styles}/>
                            </div>
                        </div>
                        <div className={styles.rowFull}>
                            <div className={styles.saveBox}>
                                <input type="submit" hidden/>
                                <Button label={'שמירה'} className={styles.saveBtn} theme={styles} onClick={this.handleSubmit} disabled={this.state.pending}/>
                            </div>
                        </div>
                    </form>
                </div>
            </Page>
        );
    }
}
module.exports = Quote
