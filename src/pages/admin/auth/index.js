import React, {PropTypes as T} from 'react';
import CSSModules from 'react-css-modules'
import styles from './style.scss'
import Input from 'react-toolbox/lib/input';
import {Button } from 'react-toolbox/lib/button';
import firebase from 'firebase'
import {observer} from 'mobx-react'
import texts from 'config/texts'
import ProgressBar from 'react-toolbox/lib/progress_bar';
import {withRouter} from 'react-router'
import {toJS} from 'mobx'
import Page from 'pages/shared/Page'
@withRouter
@CSSModules(styles)
@observer(['ui','session'])
class Admin extends React.Component {

    constructor(props) {
      super(props)

      this.state = { email: '', password: '', pending: false, };


    }
    componentWillReceiveProps(props) {
      if (props.session.isAdmin) {
            this.navigate('dashboard')
      }
    }
componentDidMount()  {
    if (toJS(this.props.session.isAdmin)) {
        this.navigate('dashboard')
    }
    document.body.scrollTop = 0;
}

toggle =  () => {
  this.setState({pending: !this.state.pending})
}
handleSubmit = (e) => {
  this.toggle();
  e.preventDefault()
  const {email,password} = this.state
  firebase.auth().signInWithEmailAndPassword(email, password)
    .then(user => {
      this.toggle()
      this.props.session.verifyAdmin(user.uid)
      .then(snap => {
        if (snap.val()) {
          this.navigate(`dashboard`)
        }
        else {
        console.log('you shouldnt be here, no logged in users bseides admin are allowed!')
        }
      })
    })
    .catch(error => {
      this.toggle()
      const errorMessage = error.message;
      this.props.ui.showError(errorMessage)
    });

}
navigate = path => {
  this.props.router.replace(`/admin/${path}`)
}
handleChange = (name, value) => {
  this.setState({...this.state, [name]: value});
};

  render() {
    return (

      <Page name={'login'}>
        {!this.state.pending ?  <form className={styles.form} onSubmit={this.handleSubmit}>

         <div className={styles.row}>
          <Input type='email' label={texts.admin.auth.email} icon='email' value={this.state.email} onChange={this.handleChange.bind(this, 'email')} className={styles.formInput} theme={styles} />
          </div>
          <div className={styles.row}>
          <Input type='password' label={texts.admin.auth.password} name='password'   icon='vpn_key' value={this.state.password} onChange={this.handleChange.bind(this, 'password')} className={styles.formInput} theme={styles} />
          </div>
          <div className={styles.submitRow}>
            <Button type="submit" label={texts.admin.auth.send} name="submit" className={styles.btn} theme={styles} value={texts.admin.auth.send} onClick={this.handleSubmit}/>
          </div>
        </form>
        :  <div className={styles.loader}><ProgressBar type="circular" mode="indeterminate" /></div>}
      </Page>
    );
  }
}

module.exports = Admin

