import React, {PropTypes as T} from 'react';
import CSSModules from 'react-css-modules'
import styles from './style.scss'
import {Button } from 'react-toolbox/lib/button';
import {withRouter} from 'react-router'
import counterpart from 'counterpart'
import Page from 'pages/shared/Page'
@withRouter
class Dashboard extends React.Component {
    state = {
        items: []
    }

    navigate = path => this.props.router.replace(`/admin/${path}`)
    render() {
        return (
          <Page name={'dashboard'}>
            <div className={styles.box}>
              <Button icon={'description'} label={counterpart('app.leads')} onClick={() => this.navigate('leads')} className={styles.btn} theme={styles}/>
              </div>
              <div className={styles.box}>
              <Button icon={'email'} label={counterpart('app.posts')}  onClick={() => this.navigate('posts')} className={styles.btn} theme={styles}/>
            </div>
            <div className={styles.box}>
            <Button icon={'group'} label={counterpart('app.groups')}  onClick={() => this.navigate('groups')} className={styles.btn} theme={styles}/>
          </div>
          </Page>

        );
    }
}

module.exports = Dashboard