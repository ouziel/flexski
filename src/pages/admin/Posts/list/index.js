import React, {PropTypes as T} from 'react';
import PageList from 'pages/shared/List'
import counterpart from 'counterpart'

class PostsList extends React.Component {

    render() {

        return (

          <PageList
            name={'posts'}
            subHeader={counterpart('posts.messages')}
            itemCaption='name'
            itemLegend='email'/>

        );
    }
}

module.exports = PostsList

