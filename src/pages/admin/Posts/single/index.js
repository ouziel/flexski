import React, {PropTypes as T} from 'react';
import counterpart from 'counterpart'
import Single from 'pages/shared/Single'
import {Button} from 'react-toolbox/lib/button';
import {withRouter, Link} from 'react-router'

@withRouter
class PostSingle extends React.Component {

    render() {

        const {id} = this.props.params
        return (

          <Single
            name={'posts' }
            prevLink={'/admin/posts'}
            id={id}
            subHeader={counterpart('posts.singlePost')}
            user='name'
            email='email'
            tel='tel'
            msg={true}
          >
          <Link to={{pathname: `admin/quote/${id}`, query: { noLead: true }}}>
          <Button label={counterpart('wizard.labels.makeQuote')} />
          </Link>
          </Single>


        );
    }
}

module.exports = PostSingle