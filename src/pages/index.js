export {default as HomePage} from './home'
export {default as ContactPage} from './contact'
export {default as AboutPage} from './about'
export {default as WizardPage} from './wizard'
export {default as UserDetailsPage} from './wizard/UserDetails'
export {default as TripDetailsPage} from './wizard/TripDetails'
export {default as AdminPage} from './admin'
// not being used because code splitting requires module.exports 