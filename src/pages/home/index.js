import React, {PropTypes as T} from 'react';
import CSSModules from 'react-css-modules'
import styles from './style.scss'
import Intro from './Intro'
import Grid from './Grid'
import Footer from './Footer'
import Carosuel from './Carosuel'

@CSSModules(styles)
class Homepage extends React.Component {

    render() {

        return (
            <div className={styles.home}>
            <div className={styles.cover}>
                <Carosuel/>
            </div>
            <Intro/>
            <div className={styles.sections}>
            <Grid/>
            </div>
              <div className={styles.bgRight}/>
            <Footer/>
          </div>
        );
    }
}
module.exports = Homepage