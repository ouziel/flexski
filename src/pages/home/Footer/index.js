import React, {PropTypes as T} from 'react';
import CSSModules from 'react-css-modules'
import styles from './style.scss'
import Socials from './socials'
export default class Footer extends React.Component {
  static propTypes = {
    socials: T.bool
  }
  static defaultProps = {
    socials: true
  }
  render() {
    const {socials} = this.props
    return (

      <section className={styles.footer}>
        <div className={styles.top}>
          {socials &&   <Socials/>}
        </div>
        <div className={styles.bottom}>

        <div>
        <span className={styles.rigt}><a  className={styles.devBrand} target="_blank" href="http:///www.esnc.io/?ref='flexski'">ESNC</a></span>
        <span className={styles.left}>Flexski 2016</span>
        </div>

        </div>

      </section>);
  }
}




