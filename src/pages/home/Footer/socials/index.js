import React, {PropTypes as T} from 'react';
import CSSModules from 'react-css-modules'
import styles from './style.scss'
import Input from 'react-toolbox/lib/input';
import {IconButton} from 'react-toolbox/lib/button';
import FacebookIcon from 'components/Svgs/facebook'
import InstaIcon from 'components/Svgs/instagram'
import YoutubeIcon from 'components/Svgs/youtube'
@CSSModules(styles)
export default class Socials extends React.Component {

  render() {
    return (

      <div className={styles.socials}>

    <a className={styles.icon} href="https://www.facebook.com/Flexski-222607504818735/" target="_blank">
      <FacebookIcon svgClass={styles.svg} circleClass={styles.FacebookCircle}/>
      </a>

      <a className={styles.icon} href="https://www.instagram.com/flexski/" target="_blank">
      <InstaIcon svgClass={styles.svg} circleClass={styles.InstaCircle}/>
      </a>
      <a className={styles.icon} href="https://www.youtube.com/channel/UCmTqQRCjEYGIFPEG7vr_LAw" target="_blank">
      <YoutubeIcon svgClass={styles.svg} circleClass={styles.YoutubeCircle}/>
      </a>
      </div>);
  }
}



