import React, {PropTypes} from 'react';
import {Button, IconButton} from 'react-toolbox/lib/button';
import styles from './style.scss'
import {Link} from 'react-router'
import Input from 'react-toolbox/lib/input';
import Dropdown from 'react-toolbox/lib/dropdown';
import {countries, resorts} from '../../wizard/TripDetails/data'
import counterpart from 'counterpart'
export default class MiniForm extends React.Component {
    constructor(props) {
      super(props)
      this.state = {
        budget: '',
        country: '',
        resort: '',
        resortList: resorts.france
      }
    }
    handleChange = (name, value) => {

        if (name === 'country') {
            const resortList = resorts[value]
            this.setState({resortList, country: value})
        }
        else {
          this.setState({[name]:value})
        }
    };

  render() {

    return (

      <div className={styles.wrapper}>

        <div className={styles.inner}>

        <div className={styles.item}>
          <span className={styles.label}>
            {counterpart('wizard.labels.country')}
          </span>
            <Dropdown
              icon="flag"
              required auto onChange={this.handleChange.bind(this, 'country')} source={countries} value={this.state.country} className={styles.countries} theme={styles}/>
          </div>
          <div className={styles.item}>
            <span className={styles.label}>
              {counterpart('wizard.labels.resort')}
            </span>
            <Dropdown
              icon="terrain"
              required auto onChange={this.handleChange.bind(this, 'resort')} source={this.state.resortList} className={styles.countries} theme={styles} value={this.state.resort}/>
        </div>

          <div className={styles.sendRow}>
          <Link to={{pathname: '/wizard/trip', query: this.state }}>
          <Button label={counterpart('app.send')} className={styles.btn}/>
          </Link>
        </div>
      </div>


    </div>);
  }
}

