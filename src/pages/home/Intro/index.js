import React, {PropTypes as T} from 'react';
import CSSModules from 'react-css-modules'
import styles from './style.scss'
import Cta from '../Cta'
export default class Intro extends React.Component {
  render() {
    return (

      <section className={styles.section}>

        <div className={styles.inner}>
          <h1 className={styles.title}>{'ברוכים הבאים ל-'}<span>Flexski</span></h1>

          <div className={styles.textBox}>
          <p className={styles.text}>
          כאן תוכלו לבנות בעצמכם חבילת סקי מתוך מבחר של אין ספור אתרים באירופה, בשלל תאריכים ובמחירים הוגנים וטובים.
          </p>
          <p className={styles.text}>
          אנו מקווים שתנצלו את השיטה שלנו בכדי לפרוץ גבולות, לגלות מקומות חדשים ובעיקר לגלוש ולעשות חיים.
          </p>
          <p className={styles.text}>
          מטרת השיטה שלנו היא להתאים לכם את חבילת הסקי המושלמת, כל מה שעליכם לעשות הוא למלא את השאלון הקצר שלנו.
          </p>
          <p className={styles.text}>
          אנחנו נחזור אליכם עם פרטי חבילת סקי תפורה בדיוק למידותיכם!
          </p>
          </div>
          <Cta/>
        </div>
      </section>);
  }
}




