import React, {Component} from 'react'
import styles from './style.scss'
import CSSModules from 'react-css-modules'
import {observer} from 'mobx-react'
const rand = (min, max) => {
    if (!max) {
        max = min;
        min = 0;
    }
    return Math.round(Math.random() * (max - min) + min);
}
class Particle {
  constructor({
    x,
    y,
    r,
    o,
    ctx,
    maxWidth,
    maxHeight,
  }) {
    this.x = x
    this.y = y;
    this.r = r;
    this.o = o
    this.ctx = ctx
    this.maxWidth = maxWidth
    this.maxHeight = maxHeight
    this.dir = (rand(0,3)) ? -1 : 1;
  }

  update() {
    this.x = this.x + .5 * this.dir * Math.random()
    this.y++;
    this.o = (rand(0,1)) ? this.o -0.005 : this.o - 0.001;
    this.r = this.r - 0.01
    if (this.r <= 0) {
      this.r = (rand(0,3)) ? 5 : 3;
    }
    if (this.o <= 0) {
      this.o = 1;
    }
    if (this.x > this.maxWidth) {
      this.x = 0;
    }
    else if (this.x <= 0) {
      this.x = this.maxWidth
    }
    if (this.y > this.maxHeight) {
      this.y = 0;
      this.o = 1;
    }
    this.draw();
  }
  draw() {

    const {
      ctx
    } = this

    ctx.beginPath()
    // ctx.fillStyle = color(this.o)
    ctx.fillStyle = `rgba(255,255,255,${this.o})`
    ctx.arc(this.x, this.y, this.r, 0, 2 * Math.PI)
    ctx.fill();

  }

}
@CSSModules(styles)
@observer(['ui'])
export default class Snow extends Component {

    constructor(props) {
        super(props);
        // this.store = this.props.store
    }
    componentDidMount() {
        const {wrapper,canvas} = this.refs;
				const MAX = 200;
        const ctx = canvas.getContext('2d');
        canvas.style.position = 'absolute'
				canvas.style.overflow = 'hidden';
				const maxWidth = wrapper.clientWidth < 1 ? window.innerWidth : wrapper.clientWidth
				const maxHeight = wrapper.clientHeight;
        canvas.width = maxWidth
        canvas.height = maxHeight
        let shapes = [];
        for (let i = 0; i < MAX; i++) {
            const shape = new Particle({
                ctx,
								maxWidth,
				maxHeight,
				x: rand(0, maxWidth),
				y: rand(0, maxHeight),
				r: rand(1, 5),
				o: Math.random()
            });
            shapes.push(shape)
            shape.draw()
        }
			const self = this;
			function loop() {
				ctx.clearRect(0,0,maxWidth,maxHeight)
				const raf = requestAnimationFrame(loop);
				self.raf = raf;
				for (let i = 0; i< MAX; i++) {
					shapes[i].update();
				}
			}

			loop();
    }
		componentWillUnmount() {
			cancelAnimationFrame(this.raf)
		}
    render() {

        return (
            <div ref="wrapper" className={styles.wrapper}>
                    <canvas ref="canvas"></canvas>
            </div>
        )
    }

}

