import React, {PropTypes} from 'react';
import CSSModules from 'react-css-modules'
import styles from './style.scss'
import { Button } from 'react-toolbox/lib/button';
import texts from 'config/texts'
const Slider = require('react-slick');
const data = [

{
  bgClass: 'bg_b',
  title: texts.pages.home.carosuel[0],
  img: require('assets/imgs/f1.jpeg'),
},
  // {
  //   bgClass: 'bg_a',
  //   title:texts.pages.home.carosuel[1],
  //   img: require('assets/imgs/help.jpg'),
  // },
  // {
  //   bgClass: 'bg_c',
  //   title: texts.pages.home.carosuel[2],
  //   img: require('assets/imgs/flex2.jpg'),
  // },
]
export default class Carosuel extends React.Component {
  render() {
    const sliderSettings = {
        dots: false,
        fade: false,
        infinite: true,
        speed: 1500,
        autoplay: false,
        slidesToShow: 1,
        draggable: false,
        autoPlaySpeed: 2500,
        slidesToScroll: 1
    };



    return (

      <Slider {...sliderSettings} className={styles.slider}>
          {data.map((x,i) => <div  key={i}>
              <div className={styles.color}>
                <div className={styles[x.bgClass]} style={{
                    backgroundImage: `url(${x.img})`
                }}>
                  {/* <div className={styles.box}>
                    <h1>{x.title}</h1>
                  </div> */}
                </div>
              </div>
          </div>)}
      </Slider>


    );
  }
}


// // import MiniForm from '../MiniForm'
// // const img = require('assets/imgs/bg2.jpg')
// // import Logo from 'components/Svgs/Logo'
// export default class Carosuel extends React.Component {
//   render() {
//
//
//     return (
//
//       <div className={styles.sliderWrapper}>
//
//         <div className={styles.logoBox}>
//           <div className={styles.slogan}>
//             <h1>סוגרים</h1>
//             <h1>סקי</h1>
//             <h1>בקלות</h1>
//           </div>
//           <Logo svgClass={styles.svg}/>
//         </div>
//         <div className={styles.bg} style={{
//             backgroundImage: `url(${img})`
//         }}/>
//         {/* <MiniForm/> */}
//       </div>
//
//
//
//     );
//   }
// }
//



