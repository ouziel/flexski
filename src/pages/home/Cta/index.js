import React, {PropTypes} from 'react';
import styles from './style.scss'
import {Button, IconButton} from 'react-toolbox/lib/button';
import CSSModules from 'react-css-modules'
import {Link} from 'react-router'
import ReactGA from 'react-ga'
import counterpart from 'counterpart'

export default class Cta extends React.Component {
  click = () => {
    ReactGA.event({
                category: 'Navigation',
                action: 'Clicked Home CTA',
            });

  }
  render() {
    return (

    <section className={styles.section}>

      <h1 className={styles.title}>{''}</h1>

      <div className={styles.ctaBox}>
      <Link to={'/wizard'} onClick={this.click}>
      <Button primary label={counterpart('wizard.makePackage')}  className={styles.ctaButton} theme={styles} />
      </Link>
      </div>

    </section>);
  }
}



