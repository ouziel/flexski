import React, {PropTypes} from 'react';
import DatesIcon from 'components/Svgs/dates'
import FlightsIcon from 'components/Svgs/flights'
import HotelIcon from 'components/Svgs/hotel'
import LocationIcon from 'components/Svgs/locations'
import TransportIcon from 'components/Svgs/transport'
import CSSModules from 'react-css-modules'
import styles from './style.scss'
import {observer} from 'mobx-react'
const Slider = require('react-slick');
import {toJS} from 'mobx'


        const data = [
          {
            title: 'תאריכים',
            text: 'לבחור כל תאריך שתרצו, ללא רכבת אווירית ולכל משך התקופה שתבקשו',
            icon: DatesIcon,
            circleClass: styles.DatesCircle,
          },
          {
            title: 'טיסות',
            text: 'לבחור בין טיסות המשך (קונקשן) לטיסה ישירה ובכך להוזיל את מחיר החבילה באופן משמעותי',
            icon: FlightsIcon,
            circleClass: styles.FlightsCircle,
          },
          {
            title: 'דיור',
            text: 'לבחור מגוון של מקומות לינה, מדירות קטנות וחמות למלונות מהודרים',
            icon: HotelIcon,
            circleClass: styles.HotelCircle,
          },
          {
            title: 'אתרים',
            text: 'אתרי הסקי הטובים בעולם. מסלולים באורך של מינימום 45 ק״מ',
            icon: LocationIcon,
            circleClass: styles.LocationCircle,
          },
          {
            title: 'תחבורה',
            text: 'לבחור בין להשכיר רכב ולהיות עצמאים או בהסעה מאורגנת שתיקח אתכם מהשדה ישירות לאתר ובחזרה',
            icon: TransportIcon,
            circleClass: styles.TransportCircle,
          }
        ]


@CSSModules(styles)
@observer(['ui'])
export default class Grid extends React.Component {
  render() {
          const {isMobile} = this.props.ui

    return (

        <section className={styles.section}>
        <h2 className={styles.title}>{'אפשרויות החבילה'}</h2>
        {/* grid  */}
        <ul className={styles.list}>

              {data.map((item,i) => {
                const Icon = item.icon
                return (
                  <li key={i} className={styles.item}>
                      <Icon  circleClass={item.circleClass}/>
                      <h3 className={styles.itemTitle}>{item.title}</h3>
                      <p  className={styles.itemText}>{item.text}</p>
                  </li>
                )
              })}

          </ul>

        </section>
    );
  }
}



