import React, {PropTypes as T} from 'react';
import CSSModules from 'react-css-modules'
import styles from './style.scss'
import counterpart from 'counterpart'

class NoMatch extends React.Component {

        render() {

            return (

                <div className={styles.wrapper}>
                  <h1>
                    {counterpart('app.noMatch')}
                  </h1>
                </div>
            );
        }
    }
module.exports = NoMatch