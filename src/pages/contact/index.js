import React, {PropTypes as T} from 'react';
import CSSModules from 'react-css-modules'
import styles from './style.scss'
import Input from 'react-toolbox/lib/input';
import {Button, IconButton} from 'react-toolbox/lib/button';
import Socials from '../home/Footer/socials'
import ContatForm from 'pages/shared/contactForm';
import firebase from 'firebase'
import omit from 'object.omit'
import {observer} from 'mobx-react'
import texts from 'config/texts'
import moment from 'moment'

// window.firebase = firebase
import Page from 'pages/shared/Page'
@CSSModules(styles)
@observer(['ui'])
class Contact extends React.Component {

    constructor(props) {
      super(props)
      this.postsRef = firebase.database().ref().child('posts')
      this.state = { postError: false, postSuccess: false, sentPost: false, name: '', phone: '', email: '', msg: '', pending: false};

    }

  handleChange = (name, value) => {
   this.setState({...this.state, [name]: value});
 };

toggle  = () => {
  this.setState({pending:!this.state.pending})

}
handleSubmit = formData => {
  this.setState({sentPost: true})
  this.toggle();
  let data = omit(formData, ['hint','approvedTerms'])
  data = {...data, date: moment().format('DD-MM-YYYY') }
  const key = this.postsRef.push().key
  const updates = {}
  updates[`posts/${key}`] = data
  firebase.database().ref().update(updates)
  .then(res => {
    this.setState({postSuccess: true})
    this.toggle()
  }).catch(err => {
    this.props.ui.showError(texts.formSubmitError)
    this.toggle()
  })
}

  render() {
    return (

        <Page name='contact'>
          {this.state.sentPost && !this.state.pending ? this.state.postSuccess ? null : <ContatForm handleSubmit={this.handleSubmit.bind(this)} pending={this.state.pending}/> :
          <ContatForm handleSubmit={this.handleSubmit.bind(this)} pending={this.state.pending}/>
        }
          <section className={styles.textInfo}>
          <p>
          {  !this.state.postSuccess  && texts.pages.contact[0] }
          {  this.state.postSuccess  && texts.pages.contact[1] }
          </p>
          <p>
          { !this.state.postSuccess && texts.pages.contact[2]  }
          { this.state.postSuccess && texts.pages.contact[3]  }
          </p>
          <div>
            <Socials/>
          </div>
        </section>
          <div className={styles.bg}/>
        </Page>


    );
  }
}

module.exports = Contact

