function focusElement(ref) {
  let element;
  try {
    // ref is a a dom node
    ref.focus()
    element = ref;
  }
  catch (err) {
    // ref is a react component
    const instance = ref.getWrappedInstance();
    element = instance.refs.input
    element.focus();

  }
  document.body.scrollTop = element.offsetTop - element.getBoundingClientRect().height
  setTimeout(() => element.blur(), 2000)
}


export default function validateElements(context,inputs) {
  const {refs,state} = context
   let shouldBreak = false;
   // fill array of false values, change them if value is valid
   let validatedFields = Array(Object.keys(refs).length).fill(false)
   let count = 0;
    for (let key in refs) {
      // loop all input rows
      const ref = refs[key]
      const input = inputs[key];
      const {validate, fields} = input
      for (let val of fields) {
        // loop all fields in this row
         const value =  state[val]
         // validate value
         if (!validate(value)) {
           // break  inner loop
           // set outter loop to break too
           // focus on the non-valid element
          //  console.log(`${key} failed validation`)
           focusElement(ref)
           shouldBreak = true;
           break;
         }
      }
      // break if anything is not valid
      if (shouldBreak) {
        break;
      }
      // if this row is cleared, set it to tru
      else {
        validatedFields[count] = true
      }
      count++;
  }
  // if all fields are true, data is valid
return validatedFields.every(x=> x === true)
}
