var counterpart = require('counterpart')
require('react-interpolate-component')

counterpart.setLocale('he')
counterpart.registerTranslations('he', {
  contact:{
    title:'יצירת קשר',
  },
  about: {
    title:'אודות החברה',
  },
  dashboard:{
    title:'לוח בקרה',
  },
  login: {
    title:'התחברות',
  },
  terms:{
    title:'תנאי שימוש'
  },
    app: {
        noPrefs: 'אין העדפה',
        approveTerms: 'הנני מאשר/ת את',
        terms: 'תנאי השימוש',
        country: 'מדינה',
        send: 'שליחה',
        noMatch: 'העמוד לא קיים במערכת',
        noPosts: 'אין מידע במערכת',
        dashboard: 'לוח בקרה',
        leads: 'פניות להצעת מחיר',
        posts: 'פניות למערכת',
        groups: 'הצעות מחיר',

    },
    posts: {
        title: 'פניות גולשים',
        singlePost: 'פרטי פנייה:',
        messages: 'פניות שנתקבלו במערכת'
    },
    passanger: {
        general: 'פרטים כללים',
        contacts: 'פרטי התקשרות',
        passportDetails: 'פרטי דרכון',
        name: 'שם מלא',
        email: 'דוא״ל',
        tel: 'טלפון',
        gender: 'מין',
        passportNum: 'מספר דרכון',
        passportExpairy: 'תוקף דרכון',
        passportBirth: 'תאריך הנפקת דרכון',
        birthday: 'תאריך לידה',
        needsSkiGear: 'מעוניין בציוד סקי',
        passportName: 'שם בלועזית',
        labels: {
            gender: {
                male: 'זכר',
                female: 'נקבה'
            }
        }
    },
    groupList: {
      title: 'הצעות מחיר',
    },  groupSingle: {
        title: 'הצעת מחיר',
      },

    groups: {
        messages:'קבוצות במערכת',
        packageReady: 'החבילה מוכנה עבורכם! אנא עברו על פרטי החבילה ומלאו את פרטי חברי הקבוצה.',
        title: 'חבילת סקי',
        members: 'חברי הקבוצה',
        package: 'פרטי חבילה',
        labels: {
            location: 'יעד',
            flightTo: 'יציאה',
            flightFrom: 'חזרה',
            flights: 'טיסות',
            departure: 'שעת המראה',
            landing: 'שעת נחיתה',
            country: 'מדינה',
            resort: 'אתר'
        }
    },
    quote: {
        makeOffer: 'טופס הצעת מחיר:',
        title: 'הצעת מחיר',
        labels: {
            extras: 'הערות',
            price: 'מחיר',
            deprartureTime: 'שעת המראה',
            landingTime: 'שעת נחיתה'
        }
    },
    leads: {
        singleLead: 'פנייה מגולש:',
        title: 'בקשות להצעת מחיר',
        messages: 'טפסים שנשלחו למערכת:'
    },

    wizard: {
      extrasHint: 'מזחלות, ספא, טיסות המשך וכו׳. רשמו כל מה שעולם בדעתכם.',
      makePackage: 'הרכבת חבילה',
      landingTime: 'שעח נחיתה',
      departureTime:'שעת המראה',
      ctaText: 'התחלת שאלון',
      explainProccess:'אופן התהליך',
      title: 'הרכבת חבילה',
      tripDetails: {
        title: 'פרטי נסיעה',
      },
      userDetails:{
        title:'פרטים אישיים',
      },
        labels: {
            makeQuote: 'הפק הצעת מחיר',
            msg: 'בקשות נוספות',
            accomndation: 'דיור',
            budget: 'תקציב',
            tel: 'טלפון',
            budget: 'תקציב',
            endDate: 'תאריך חזרה',
            startDate: 'תאריך יציאה',
            dates: 'תאריכים',
            isFlexable: 'גמיש בתאריכים',
            transport: 'תחבורה',
            country: 'מדינה',
            resort: 'אתר',
            children: 'ילדים',
            adults: 'מבוגרים',
            people: 'כמות אנשים',
            destination: 'יעד'
        },
        accomndation: {
            apt: 'דירה',
            hotel: 'מלון',
            noPrefs: 'אין העדפה'
        },
        isFlexable: {
            true: 'כן',
            false: 'לא'
        },
        transport: {
            noPrefs: 'אין העדפה',
            car: 'מכונית',
            publicRide: 'הסעה',
            privateRide: 'הסעה פרטית'
        }
    }
});

const texts = {
    labels: {
        tel: 'טלפון',
        budget: 'תקציב',
        accomndation: 'דיור',
        transport: 'תחבורה',
        children: 'ילדים',
        adults: 'מבוגרים'
    },
    formSubmitError: 'התרחשה תקלה בשליחת הטופס',
    contactUs: 'יצירת קשר',
    extrasHint: 'מזחלות, ספא, טיסות המשך וכל מה שעולה בדעתכם',
    home: 'בית',
    wizard: 'שאלון',
    about: 'אודות',
    aboutUs: 'אודות החברה',
    pages: {
        about: [
            'החברה הוקמה בשנת 2016 ונועדה לפרוץ את הגבולות לגולש הישראלי.', 'ההתמחות שלנו זה בניית חופשות סקי באופן אישי. אנו נבנה לכם את חופשת הסקי לפי יכולתכם ורצונותיכם.', 'לסוכנים שלנו יש ניסיון של מעל 7 שנים בעולם הסקי, והם יעזרו לכם להרכיב את החבילה שתמיד חלמתם עליה.', '          כמו כן, אנחנו ניהיה זמינים במהלך חופשת הסקי שלכם למקרה שתצטרכו סיוע מצידנו.', '  אנחנו עושים זאת מתוך אהבת אמת לספורט ושואפים להגדיל את הפופולאריות של הסקי בישראל!'
        ],
        contact: [
            'אנחנו תמיד שמחים לשמוע מה יש לגולשים להגיד.', 'תודה רבה על פנייתכם', 'לכל בקשה, רעיון, המלצה או שיגעון - דברו איתנו, אנחנו נשמח לעזור!', 'נציגנו יצרו עמכם קשר בהקדם!'
        ],
        home: {
            carosuel: [
                'החופשה הנוחה עבורכם', 'ייעוץ וסיוע אישי לכל גולש', 'מתגמשים בשבילך'
            ],
            cta: 'התחלת שאלון'
        }

    },
    admin: {
        dashboard: 'ניהול',
        auth: {
            login: 'התחברות',
            email: 'דוא״ל',
            password: 'סיסמא',
            send: 'שליחה'
        }
    },
    leads: {
        title: 'בקשות להצעת מחיר',
        messages: 'פניות שהתקבלו במערכת:'
    }
}
export default texts