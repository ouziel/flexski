import React, {PropTypes} from 'react';
import ReactDOM from 'react-dom';
import FlexskiApp from 'components/routes';
// const hideLoader = require('appUtil/hideLoader').default()


ReactDOM.render(
<FlexskiApp/>,
document.getElementById('root'));