var path = require('path')
var precss = require('precss')
var autoprefixer = require('autoprefixer')
var ExtractTextPlugin = require('extract-text-webpack-plugin')
var HtmlWebpackPlugin = require('html-webpack-plugin')
var FaviconsWebpackPlugin = require('favicons-webpack-plugin')
var CommonsChunkPlugin = require('webpack/lib/optimize/CommonsChunkPlugin')
var faviconPath = path.resolve(__dirname, './src/assets/imgs/logo.png')
var SitemapPlugin = require('sitemap-webpack-plugin')

var sitemapPaths = [
  '/about/',
  '/contact/',
  '/wizard/'
]

// var fs = require('fs')
// // var routeComponentRegex = /pages[\/\\]([^\/\\]+[\/\\]?[^\/\\]+).js/
// fs.readdir('./src/pages/admin/Groups', (err, files) => {
//   files.forEach(file => {
//     console.log(file)
//   })
// })

var pages = [
  `${path.resolve(__dirname, './src/pages/terms/index.js')}`,
  `${path.resolve(__dirname, './src/pages/about/index.js')}`,
  `${path.resolve(__dirname, './src/pages/contact/index.js')}`,
  `${path.resolve(__dirname, './src/pages/home/index.js')}`,
  `${path.resolve(__dirname, './src/pages/wizard/index.js')}`,
  `${path.resolve(__dirname, './src/pages/wizard/TripDetails/index.js')}`,
  `${path.resolve(__dirname, './src/pages/wizard/UserDetails/index.js')}`,
  `${path.resolve(__dirname, './src/pages/admin/Leads/list/index.js')}`,
  `${path.resolve(__dirname, './src/pages/admin/Leads/quote/index.js')}`,
  `${path.resolve(__dirname, './src/pages/admin/Leads/single/index.js')}`,
  `${path.resolve(__dirname, './src/pages/admin/Posts/list/index.js')}`,
  `${path.resolve(__dirname, './src/pages/admin/Posts/single/index.js')}`,
  `${path.resolve(__dirname, './src/pages/admin/Dashboard/index.js')}`,
  `${path.resolve(__dirname, './src/pages/admin/auth/index.js')}`,
  `${path.resolve(__dirname, './src/pages/admin/Groups/Single/index.js')}`,
  `${path.resolve(__dirname, './src/pages/admin/Groups/AdminList/index.js')}`
]

module.exports = {
  entry: './src/main.js',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'bundle.js',
    chunkFilename: '[id].chunk.js'
  },
  module: {
    loaders: [

      {
        test: /\.(js|jsx)$/,
        loader: 'babel-loader',
        exclude: [
          /node_modules/, pages
        ],
        query: {
          plugins: [
            'transform-decorators-legacy', 'transform-runtime'
          ]
                    //     presets: ['es2015', 'react', 'stage-2']
        }
      }, {
                // &name=[1]&regExp=pages\/(.*)\/\/.*$'
        test: /\.(js|jsx)$/,
        include: pages,
        loaders: ['bundle?lazy', 'babel']
      }, {
        test: /\.(css|scss)$/,
        loaders: ['style', 'css?modules&importLoaders=1&localIdentName=[path]___[name]__[local]___[hash:base64:5]', 'postcss', 'sass']
      }, {
        test: /\.(svg)$/,
        loader: 'file?name=svg/[name].[ext]'
      }, {
        test: /\.(eot|ttf|woff|woff2)$/,
        loader: 'file?name=fonts/[name].[ext]'
      }, {
        test: /\.(png|jpg|jpeg|gif|woff)$/,
        loader: 'url-loader?limit=8192'
      }
    ]
  },
  eslint: {
    configFile: './.eslintrc'
  },
  plugins: [
    new SitemapPlugin('https://www.flexski.co.il', sitemapPaths, 'sitemap.xml'),
    new FaviconsWebpackPlugin(faviconPath),
    new CommonsChunkPlugin('commons.chunk.js'),
    new ExtractTextPlugin('style.css'),
    new HtmlWebpackPlugin({
      template: __dirname + '/src/index.html',
      filename: 'index.html',
      inject: 'body'
    })
  ],
  resolve: {
    extensions: [
      '', '.js', '.jsx'
    ],
    modulesDirectories: [
      'node_modules', 'src/assets/img'
    ],
    root: path.resolve(__dirname),
    alias: {
      components: path.join(__dirname, '/src/components'),
      pages: path.join(__dirname, '/src/pages'),
      styles: path.join(__dirname, '/src/styles'),
      config: path.join(__dirname, '/src/config'),
      stores: path.join(__dirname, '/src/stores'),
      assets: path.join(__dirname, '/src/assets'),
      appUtil: path.join(__dirname, '/src/util')
    }
  },
  sassLoader: {
    data: '@import "' + path.resolve(__dirname, 'src/styles/helpers.scss') + '";'
  },
  postcss: function () {
    return [
      require('postcss-rtl'),
      precss,
      autoprefixer({browsers: ['last 4 versions']})
    ]
  },
  devServer: {
    historyApiFallback: true,
    contentBase: './',
    port: 6090,
    stats: {
      colors: true,
      hash: false,
      version: false,
      timings: false,
      assets: false,
      chunks: false,
      modules: false,
      reasons: false,
      children: false,
      source: false,
      errors: true,
      errorDetails: false,
      warnings: false,
      publicPath: true
    }
  }
}
